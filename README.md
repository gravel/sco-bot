# Sessioncommunities.online bot

## Prerequisites

- [Node.js](https://nodejs.org/)
- [nvm (Node Version Manager)](https://github.com/nvm-sh/nvm/)

## Usage

See [src/opts.ts](src/opts.ts).

## Commands

See [src/text/bot/command-details.ts](src/text/bot/command-details.ts).

## Credits

Based on <https://github.com/hesiod-project/node-session-client>

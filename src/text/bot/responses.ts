import { BotCommandDetails } from "../../types/bot-command";
import { concatenate, noindent, onlyif, pluralize } from "../../utils/text";
import { COMMAND_DETAILS } from "./command-details";

export class TextResponses {
    public static reportNumberCommunitiesFound(numFound: number, numShown: number): string {
        if (numFound === 0) {
            return "i don't know about any communities like that, sorry";
        }

        return pluralize`i found ${numFound} communities ${onlyif(
            numFound > numShown
        )`(showing top ${numShown})`}`;
    }

    public static formatCommandDetails(
        { synopsis, description, summary }: BotCommandDetails,
        commandPrefix: string
    ): string {
        if (!synopsis) {
            throw new Error("Cannot display command without synopsis");
        }
        const synopsisFormatted = synopsis.replace(/^/gm, commandPrefix).replace(/^.+$/gm, "`$&`");
        return noindent`
        ${synopsisFormatted}
        ${onlyif(summary !== undefined)`

        ${summary}
        `.trim()}
        ${onlyif(description !== undefined)`
        ::::

        ${description}
        `.trim()}
    `.trim();
    }

    public static mentionResponse(commandPrefix: string): string {
        return concatenate(
            "hi, i'm rocky.",
            `reply with "${commandPrefix}about" to learn more about me or "${commandPrefix}help" to see my list of commands!`
        );
    }

    public static botInfo(commandPrefix: string): string {
        return noindent`
            hi, i'm rocky!

            i'm made by gravel and i'm the official bot of sessioncommunities.online.

            i can help you find communities, or help out in a community you moderate.

            to see my list of commands, type "${commandPrefix}help"!
        `;
    }

    public static helpResponseInCommunity(): string {
        return `dm my id "rocky" for help`;
    }

    public static unknownCommandResponse(
        commandPrefix: string,
        isPublicCommunity: boolean
    ): string {
        return isPublicCommunity
            ? `unknown command. dm me "${commandPrefix}help" at @rocky to see my commands`
            : `unknown command. try "${commandPrefix}help"`;
    }

    public static communityListingLink(): string {
        return "find session communities at https://sessioncommunities.online/";
    }

    public static botAnnounceMessage(commandPrefix: string): string {
        return noindent`
            hi, i'm the bot.
            try \`${commandPrefix}help\` to see my commands. if you are on the bot's allowlist, you can also verify your identity in rooms where you are not staff with \`${commandPrefix}challenge start\`.
        `;
    }

    public static pingResponse() {
        return "pong";
    }

    public static identityChallengeSuccess() {
        return "thanks, identity matched";
    }

    public static identityChallengeExpiry() {
        return "sorry, that challenge expired";
    }

    public static identityChallengeInstructions(commandPrefix: string) {
        const submitCommandSynopsis = COMMAND_DETAILS.challenge.synopsis
            .split("\n")
            .filter((x) => x.includes("submit"))[0];

        return noindent`
            send this command in any community where i'm active to confirm your identity there:

            \`${commandPrefix}${submitCommandSynopsis}\`

            your token is:
        `;
    }

    public static identityChallengeWrongSubmissionLocation(): string {
        return "please submit your challenge in a community instead";
    }

    public static quoteRoomDescription(roomName: string, description: string | undefined) {
        if (description !== undefined) {
            return noindent`
                description for community '${roomName}':

                ${description}

                ----
            `;
        } else {
            return "no description available";
        }
    }

    public static offerMoreSearchResults(searchTerm: string) {
        return `more results at https://sessioncommunities.online/#q=${encodeURIComponent(
            searchTerm
        )}`;
    }

    public static donationReminder(commandPrefix: string, inPublicCommunity: boolean) {
        if (inPublicCommunity) {
            return `\n(dm \`${commandPrefix}donate\` to "rocky" = ❤️)`;
        } else {
            return `\n(reply \`${commandPrefix}donate\` to see xmr & btc addresses)`;
        }
    }

    public static donationInfoResponses() {
        return [
            "send monero to:",
            "88y1HDjnzHJaWAh5Eu5JEJUXaN1nQR95GLD8mzA8iPQ5ccXbC4AUgEL4ricouvVUdRf9pwL5FUqTFNN7ewFeXC3EA6q1WQ1",
            "send bitcoin to:",
            "bc1qynyu34ttwhthm5mjc7tck75wenvthj6stjuz96",
            "to verify these addresses, visit https://codeberg.org/gravel/gravel/#support or message @gravel",
        ];
    }

    public static showDefinitionEntries(definitionIdentifier: string, entries: readonly string[]) {
        return noindent`

    '${definitionIdentifier}':

    ${entries
        .map((entry, index) => `${index + 1}) ${entry}${index == entries.length - 1 ? "." : ","}\n`)
        .join("")}

        `;
    }

    public static nullTimestampWarning() {
        return concatenate(
            "your device does not add a timestamp to outgoing messages.",
            "this may result in me spamming you: due to a bug,",
            "timestamps are needed to distinguish old messages from new ones."
        );
    }
}

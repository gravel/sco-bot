import { BotCommandDetails } from "../../types/bot-command";
import { noindent } from "../../utils/text";

export const COMMAND_DETAILS = {
    info: {
        synopsis: noindent`
        info
        about
        `,
        summary: "learn more about me!",
    },
    rooms: {
        synopsis: "rooms SEARCH-TERM...",
        summary: "find communities matching the given search term",
        description: noindent`
            shows the top few matching communities.
            name and link to sessioncommunities.online is displayed.
            whole word matches are preferred.
            language flags work as well, as long as it's not a doppelganger flag.
        `,
    },
    description: {
        synopsis: noindent`
            description
            desc
            rules
        `,
        summary: "display the current community's description",
    },
    challenge: {
        synopsis: noindent`
            challenge start
            challenge submit TOKEN
        `,
        summary: "authenticate yourself in a community",
        description: noindent`
            this command is only useful if you need to execute commands in a community where you are not staff.
            this command requires you are on the bot's global allowlist.
            the authentication challenge starts with "challenge start" in DMs and ends with "challenge submit <TOKEN>" in the target community.
            it links your blinded Session ID to your unblinded Session ID, which is on the allowlist.
        `,
        unlisted: true,
    },
    ping: {
        synopsis: "ping",
        summary: "make me say 'pong'",
        unlisted: true,
    },
    link: {
        synopsis: "link",
        summary: "invite people to sessioncommunities.online",
    },
    help: {
        synopsis: "help",
        summary: "(DMs only) show a list of available commands",
    },
    allowlist: {
        synopsis: noindent`
            allowlist add ID
            allowlist remove ID
        `,
        summary: "(admin only) set who can get responses from the bot in chat",
        unlisted: true,
    },
    announce: {
        synopsis: "announce ID",
        summary: "(admin only) sends the given session ID an introductory message",
        unlisted: true,
    },
    whatis: {
        synopsis: noindent`
            w|whatis|what-is [NAMESPACE/]KEY
        `,
        summary: "show definitions for KEY",
    },
    definition: {
        synopsis: noindent`
            definition add [NAMESPACE/]KEY DEFINITION
            definition remove [NAMESPACE/]KEY NUMBER
        `,
        summary: "add or remove definitions",
        description: noindent`
            community staff can use this command, provided that the community can edit the target definition namespace.

            when no namespace is provided, the community's default namespace is used, if set.
        `,
    },
    definitionNamespace: {
        synopsis: noindent`
            defns add NAMESPACE [NAMESPACES...]
            defns remove NAMESPACE [NAMESPACES...]
            defns set-public NAMESPACE true|false
            defns size NAMESPACE
            defns info NAMESPACE
            defns list NAMESPACE
            defns list
        `,
        unlisted: true,
    },
    definitionNamespaceCommunity: {
        synopsis: noindent`
            defnsc read COMMUNITY|. NAMESPACE [true|false]
            defnsc write COMMUNITY|. NAMESPACE [true|false]
            defnsc set-default COMMUNITY|. NAMESPACE
        `,
        unlisted: true,
    },
    donate: {
        synopsis: "donate",
        summary: "show donation addresses",
    },
} satisfies Record<string, BotCommandDetails>;

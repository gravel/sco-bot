import minimist from "minimist";
import { ProgramOptions } from "./types/opts";

/**
 * Display command-line usage string.
 */
export function showHelp() {
    console.log(
        `
Usage: node dist/src/main.js [OPTION...] | npm run start -- [OPTION...]

        --admin=ID          Let this Session ID give privileged commands
    -h, --help              Show this help message
        --clear-allowlist   Remove all users previously allowed to use commands in chat
        --no-autojoin       Do not automatically join any Communities
        --display-name=NAME Override the default bot display name

`.trim()
    );
}
/**
 * Configuration for accepted command-line options
 */
export const OPTS: minimist.Opts = {
    alias: {
        h: ["help"],
    },
    boolean: ["h", "help", "clear-allowlist", "autojoin"],
    string: ["admin", "display-name"],
    default: {
        autojoin: true,
    },
    unknown(arg: string) {
        console.error(`Unknown argument: ${arg}`);
        process.exit(1);
    },
};

/**
 * Produce overrides for particular user configuration options based on command-line arguments.
 * @param args Parsed command-line arguments
 * @returns Object containing overriding configuration options.
 */
export function parseUserStateOverridesFromArgs(args: ProgramOptions): Partial<UserState> {
    const overrides: Partial<UserState> = {};
    if ("display-name" in args) {
        overrides.displayName = args["display-name"];
    }
    if (args.autojoin === false) {
        overrides.communityAutoJoinList = [];
    }
    return overrides;
}

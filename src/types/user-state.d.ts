type BotConfig = {
    displayName?: string;
    communityAutoJoinList?: string[];
    commandPrefix: string;
    botExternalVerificationChallenges?: SimpleTextCommand[];
};

type SimpleTextCommand = {
    commandText: string;
    responseText: string;
};

type SessionClientIdentity = {
    seed?: string | undefined;
    keypair?:
        | {
              privKey: Buffer;
              pubKey: Buffer;
          }
        | undefined;
    displayName?: string | undefined;
    avatarFile?: string | undefined;
};

type ComplexKeys<T> = NonNullable<
    {
        [P in keyof T]-?: T[P] extends any[] | undefined
            ? P
            : T[P] extends Record<any, any> | undefined
            ? P
            : never;
    }[keyof T]
>;
type RequireComplexKeys<T> = T & Required<Pick<T, ComplexKeys<T>>>;
type RequiredBotConfig = RequireComplexKeys<BotConfig>;

type UserState = {
    seed: string | null;
    avatarFileName?: string;
    /**
     * Last inbox state.
     */
    lastHash: string | null;
    allowList: UserAllowList | null;
    userDefinitions: UserDefinitionConfig;
} & RequiredBotConfig;

type UserDefinitionConfig = {
    namespaces: Record<string, UserDefinitionNamespaceConfig>;
    communities: Record<string, UserDefinitionCommunityConfig>;
    definitions: UserDefinitionNamespaces;
};

type UserDefinitionNamespaceConfig = {
    canBeChangedByCommunities: string[];
    canBeReadByCommunities: string[];
    canBeReadByAnyone: boolean;
};

type UserDefinitionCommunityConfig = {
    defaultNamespace: string;
};

type UserDefinitionNamespaces = {
    [namespace: string]: UserNamespaceDefinitions;
};

type UserNamespaceDefinitions = {
    [definitionKey: string]: UserDefinitionType;
};

type UserDefinitionType = {
    entries: string[];
};

type UserAllowList = Record<string, UserBlindings>;

/**
 * Record of blinded ID for each SOGS pubkey.
 */
interface UserBlindings {
    [sogsPubkey: string]: string;
}

type Dict<T> = Record<string, T>;

type DeepReadonly<T> = {
    readonly [P in keyof T]: DeepReadonly<T[P]>;
};

import { Long } from "protobufjs";
import { DataMessage, SessionProfile } from "./session";
import { SessionOpenGroupV3Room } from "./session-client";

interface SessionPrivateMessage extends Omit<DataMessage, "expireTimer"> {
    /** User ID. */
    source: string;
}

interface SessionMessageContent {
    dataMessage: DataMessage;
}

interface SessionCommunityMessage {
    serverURL: string;
    room: string;
    id: number;
    roomHandle: SessionOpenGroupV3Room;
    /** Own unblided Session ID */
    destination: string;
    timestamp?: Long | number | null;
    existedBy: number;
    content: SessionMessageContent;
    verified: boolean;
    reactions: Record<string, unknown> | undefined;
    profile: SessionProfile;
    /** Session ID */
    source: string;
}

interface SessionCommunityContentMessage extends SessionCommunityMessage {
    body: string;
}

interface SessionMessageEdit extends SessionCommunityMessage {
    edited: number;
    deleted?: boolean;
}

type SessionMessage = SessionPrivateMessage | SessionCommunityContentMessage | SessionMessageEdit;

type SessionPrivateMessageSendResult = boolean;

type SessionOpenGroupV3MessageSendResult = number | boolean;

type SessionMessageSendResult =
    | SessionPrivateMessageSendResult
    | SessionOpenGroupV3MessageSendResult;

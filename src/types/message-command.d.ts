import { SessionMessage } from "./session-message";

/**
 * Specifies whether the given message is formulated as a command.
 * Used to distinguished commands from helpless mentions.
 */
type MessageCommandKind = "command" | "text";

type MessageCommand = {
    kind: MessageCommandKind;
    message: SessionMessage;
};

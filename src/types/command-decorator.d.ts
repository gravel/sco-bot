import { BotCommandRegistry } from "../utils/commands/command-registry";
import { CommandHandler } from "./bot-command";

type Rewriter<T> = (originalObject: T) => T;

type OptionalRewriter<T> = (originalObject: T) => T | void;

type Predicate<T> = (item: T) => boolean;

type BotCommandDecorator = (
    target: CommandHandler,
    context: ClassMethodDecoratorContext<BotCommandRegistry, CommandHandler> & {
        static: false;
    }
) => CommandHandler;

type DefaultDecoratorId =
    | "defaultThrottling"
    | "donationReminder"
    | "roomDisclaimer"
    | "nullTimestampWarning";

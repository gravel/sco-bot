import { SOGSRoom } from "./session";

type SCOServersJSON = SCOServer[];

interface SCOServer {
    base_url: string;
    pubkey: string;
    rooms: SCORoom[];
    server_id: string;
}

interface SCORoomTag {
    text: string;
    type: "user" | "reserved" | "warning";
}

interface SCORoom extends SOGSRoom {
    language_flag?: string;
    string_tags: string[];
    tags: SCORoomTag[];
}

interface SCORoomWithServer extends SCORoom {
    server: Omit<SCOServer, "rooms">;
}

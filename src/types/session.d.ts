import { Long } from "protobufjs";

interface SessionProfileLocal {
    displayName: string;
    avatar: string;
}

interface SessionProfile {
    displayName: string;
    avatar: {
        url: string | undefined;
        key: unknown | undefined;
    };
}

interface DataMessage {
    attachments: unknown[];
    contact: unknown[];
    preview: unknown[];
    body: string;
    profileKey: Uint8Array;
    timestamp: Long;
    profile: SessionProfile;
    expireTimer: number;
}

interface SOGSRoom {
    active_users?: number;
    active_users_cutoff?: number;
    token: string;
    name?: string;
    admins?: string[];
    moderators?: string[];
    created?: number;
    description?: string;
    image_id?: number;
    read?: boolean;
    write?: boolean;
    upload?: boolean;
}

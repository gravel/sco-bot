import type { ParsedArgs } from "minimist";

type ProgramOptions = ParsedArgs;

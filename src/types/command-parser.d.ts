/**
 * Represents an inclusive word array range covered by quotes.
 */
interface QuoteRangeInclusive {
    /** Array index of word starting quotation. */
    from: number;
    /** Array index of word ending quotation. */
    to: number;
    /** Character used to quote range. */
    quoteCharacter: string;
}

import { SOGSRoom } from "./session";

interface SessionOpenGroupV3Server {
    serverURL: string;
    serverPubkeyHex: string;
    pollServer: boolean;
    rooms: unknown;
    caps: unknown[];
    blinded: boolean;
    useV3: boolean;
    capsResolve: Function;
    capsReady: Promise<unknown>;
    lastInboxId: unknown;
    lastOutboxId: unknown;
    anyKeypair: unknown;
}

interface SessionOpenGroupV3Room {
    server: SessionOpenGroupV3Server;
    keypair: {
        privKey: Buffer;
        pubKey: Buffer;
        publicKeyHex: string;
        ed25519KeyPair: unknown;
    };
    room: string;
    lastId: number;
    token: string;
    roomData: SOGSRoom;
    displayName: string;
    profileInfo: unknown;
}

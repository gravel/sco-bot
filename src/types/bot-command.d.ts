import type SessionClient from "../../node-session-client/session-client";
import { UserStateManager } from "../utils/state/user-state";
import { BotCommandRegistry } from "../utils/commands/command-registry";
import { DefaultDecoratorId } from "./command-decorator";
import { MessageCommand } from "./message-command";
import { SessionProfile } from "./session";
import { SessionMessage, SessionMessageSendResult } from "./session-message";
import { SessionOpenGroupV3Room } from "./session-client";

/**
 * Context of incoming message..
 */
interface BotMessageContext {
    /**
     * Client used by the bot to interact with Session.
     */
    client: SessionClient;
    command: MessageCommand;
    message: SessionMessage;
    /**
     * SessionProfile of message author.
     */
    profile: SessionProfile;
    /**
     * Room message was sent in, if applicable.
     */
    room: SessionOpenGroupV3Room | null;
    /**
     * Session ID of author.
     */
    author: string;
    /**
     * Send a message in the channel that the command came in.
     * @param message Text body of response message.
     * @param options node-session-client message options
     */
    respond(message: string, options?: any): Promise<SessionMessageSendResult>;

    /**
     * Manager of user state belonging to current session.
     */
    userState: UserStateManager;
}

/**
 * Function responding to command invocation.
 */
type CommandHandler = (this: BotOpaque, ctx: BotMessageContext, ...args: string[]) => Promise<void>;

/**
 * Function responding to non-command messages.
 */
type TextMessageHandler = (this: BotOpaque, ctx: BotMessageContext) => Promise<void>;

type MessageHandler = CommandHandler | TextMessageHandler;

type BotOpaque = any;

/**
 * Factory function producing bots which add commands to the given controller.
 */
type BotFactory = (commandController: BotCommandRegistry) => Promise<BotOpaque>;

interface BotCommandDetails {
    synopsis?: string;
    summary?: string;
    description?: string;
    unlisted?: boolean;
}

interface BotCommandProps {
    defaultDecoratorExceptions?: DefaultDecoratorId[];
}

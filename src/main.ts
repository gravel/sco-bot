import SessionClient from "../node-session-client/session-client.js";
import { ConversationHandler } from "./conversation.js";
import { UserStateManager } from "./utils/state/user-state.js";
import minimist from "minimist";
import { singleton } from "./utils/collections.js";
import { PrivilegeManager } from "./utils/auth/privilege-manager.js";
import { ProgramOptions } from "./types/opts.js";
import { botFactory } from "./bot.js";
import { OPTS, parseUserStateOverridesFromArgs, showHelp } from "./opts.js";

/**
 * Launch the chatbot.
 */
async function main() {
    const args = minimist<ProgramOptions>(process.argv.slice(2), OPTS);

    // Display usage string
    if (args.help) {
        showHelp();
        return;
    }

    // Create client for communication with Session nodes
    const sessionClient = new SessionClient();

    // Initialize privilege manager with list of passed admins
    const privilegeManager = PrivilegeManager.createInstance({
        admins: singleton<string>(args.admin),
    });

    // Find options overriding user state
    const userStateOverrides = parseUserStateOverridesFromArgs(args);

    // Track user state and save changes to disk
    const userStateManager = await UserStateManager.createInstance(
        sessionClient,
        privilegeManager,
        userStateOverrides
    );

    // Purge allowlist if specified
    if (args["clear-allowlist"]) {
        userStateManager.clearAllowList();
    }

    // Log Session ID
    userStateManager.echoIdentity();

    // Handle incoming messages.
    await ConversationHandler.createInstance(sessionClient, userStateManager, botFactory);

    // Initiate connection to service nodes.
    await sessionClient.open();

    console.log("Ready");
}

main();

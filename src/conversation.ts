import type SessionClient from "../node-session-client/session-client";
import { BotFactory, BotMessageContext } from "./types/bot-command";
import { BotCommandRegistry } from "./utils/commands/command-registry";
import { CommandStringParser } from "./utils/commands/command-parser";
import { trimAffix } from "./utils/text";
import { UserStateManager } from "./utils/state/user-state";
import { isDirectMessage } from "./utils/session/message-context";
import { SessionMessage } from "./types/session-message";
import { MessageCommand } from "./types/message-command";
import { MessageAgeFilter } from "./utils/messages/message-age-filter";

/**
 * Handles and responds to incoming chat messages.
 */
export class ConversationHandler {
    /**
     * Provider of the bot's command database.
     */
    private commandController!: BotCommandRegistry;

    private messageAgeFilter = new MessageAgeFilter();

    private constructor(
        private readonly sessionClient: SessionClient,
        private readonly userStateManager: UserStateManager
    ) {
        sessionClient.on("messages", (messages: SessionMessage[]) => {
            this.receiveMessages(messages);
        });
    }

    public static async createInstance(
        sessionClient: SessionClient,
        userStateManager: UserStateManager,
        botFactory: BotFactory
    ) {
        const conversationHandler = new ConversationHandler(sessionClient, userStateManager);
        conversationHandler.commandController = await BotCommandRegistry.createInstance(
            conversationHandler.sessionClient,
            botFactory,
            userStateManager
        );
        return conversationHandler;
    }

    private receiveMessages(messages: (SessionMessage | null)[]) {
        let messageBatchIsRecycled = false;
        let nonEmptyMessageIsRecycled = false;
        for (const message of messages) {
            if (message && this.messageAgeFilter.isMessageRecycled(message)) {
                messageBatchIsRecycled = true;
                if ('body' in message && !!message.body) {
                    nonEmptyMessageIsRecycled = true;
                }
            }
        }
        if (messageBatchIsRecycled) {
            console.log(`[recycle] throwing out ${messages.length} messages`);
            if (!this.messageAgeFilter.inGracePeriod() && nonEmptyMessageIsRecycled) {
                console.log("[recycle] restarting grace period");
                this.messageAgeFilter.startGracePeriod();
            }
            return;
        }
        for (const message of messages) {
            if (message === null) {
                continue;
            }
            const command = this.parseMessageCommand(message);
            if (command === null) {
                continue;
            }
            this.processCommand(command);
        }
    }

    private processCommand(command: MessageCommand) {
        if (command.kind == "text") {
            this.runTextCommand(command);
        } else if (command.kind == "command") {
            this.runCommand(command);
        }
    }

    /**
     * Creates a context of an incoming command for use in command handlers.
     * @param messageCommand
     * @returns Environment and actions related to incoming command.
     */
    private assembleCommandContext(messageCommand: MessageCommand): BotMessageContext {
        const { message } = messageCommand;
        return {
            client: this.sessionClient,
            command: messageCommand,
            message,
            profile: message.profile,
            room: "room" in message ? message.roomHandle : null,
            author: message.source,
            respond: async (response: string, options: any = {}) => {
                if ("room" in message) {
                    return this.sessionClient.sendOpenGroupV3Message(
                        message.roomHandle,
                        response,
                        options
                    ) as Promise<number>;
                } else {
                    return this.sessionClient.send(message.source, response) as Promise<boolean>;
                }
            },
            userState: this.userStateManager,
        };
    }

    private runCommand(messageCommand: MessageCommand) {
        if (!("body" in messageCommand.message)) {
            return;
        }

        const commandPrefix = this.userStateManager.getCommandPrefix();
        const commandString = trimAffix(messageCommand.message.body, commandPrefix, "prefix");
        const commandContext = this.assembleCommandContext(messageCommand);
        const commandArguments = CommandStringParser.parseCommandString(commandString);
        if (commandArguments === null) {
            console.error("command given with invalid quoting");
            return;
        }
        const commandName = commandArguments[0];

        if (this.commandController.hasCommand(commandName)) {
            return this.commandController.runCommand(commandName, commandContext, commandArguments);
        } else {
            this.onMissingCommand(commandName, commandContext, commandArguments);
        }
    }

    private onMissingCommand(
        commandName: string,
        commandContext: BotMessageContext,
        commandArguments: string[]
    ) {
        this.commandController.runMissingCommandResponse(commandContext, ...commandArguments);
        console.log(`Non-existent command called: ${commandName}`);
    }

    private runTextCommand(messageCommand: MessageCommand) {
        if (!("body" in messageCommand.message)) {
            return;
        }

        const commandContext = this.assembleCommandContext(messageCommand);
        const textCommand = messageCommand.message.body;

        if (this.commandController.hasTextCommand(textCommand)) {
            return this.commandController.runTextCommand(textCommand, commandContext);
        } else {
            this.commandController.runDefaultTextResponse(commandContext);
        }
    }

    private messageIsDirectedAtUs(message: SessionMessage): boolean {
        // TODO: Detect blinded ID
        if (!("body" in message)) {
            return false;
        }
        return (
            isDirectMessage(message) || message.body.includes(`@${this.sessionClient.ourPubkeyHex}`)
        );
    }

    private parseMessageCommand(message: SessionMessage): MessageCommand | null {
        const commandPrefix = this.userStateManager.getCommandPrefix();
        if (!("body" in message)) {
            return null;
        }
        if (message.body.startsWith(commandPrefix)) {
            return {
                kind: "command",
                message,
            };
        } else if (this.messageIsDirectedAtUs(message)) {
            return {
                kind: "text",
                message,
            };
        }
        return null;
    }
}

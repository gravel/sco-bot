import { BotMessageContext, BotOpaque } from "./types/bot-command";
import { BotCommandRegistry } from "./utils/commands/command-registry";
import { isDirectMessage } from "./utils/session/message-context";
import { CommunityRooms } from "./utils/communities/community-rooms";
import { enumerate, noindent, pluralize } from "./utils/text";
import { COMMAND_DETAILS } from "./text/bot/command-details";
import { TextResponses } from "./text/bot/responses";
import { BaseBot } from "./utils/bot-base";
import { isValidUnblindedSessionID } from "./utils/session/credentials";
import { CommandExecutionError } from "./utils/errors";
import { UserDefinitionManager } from "./utils/state/user-defs";

/**
 * Max number of room details displayed at once.
 */
const getMaxRooms = (directMessage: boolean) => (directMessage ? 5 : 2);

/**
 * Create a new Bot instance and register bot commands.
 * @param commands Controller to register commands with.
 * @returns Promise of opaque Bot object.
 */
export async function botFactory(commands: BotCommandRegistry): Promise<BotOpaque> {
    class Bot extends BaseBot {
        /**
         * Create a bot instance and initialize its internal state.
         * @returns Opaque bot instance
         */
        static async createInstance(): Promise<Bot> {
            const bot = new Bot();
            await bot.initialize();
            commands.addSimpleTextCommands();
            commands.addDefaultDecorator("defaultThrottling", commands.throttlePerUser(5e3));
            commands.addDefaultDecorator("donationReminder", commands.addDonationReminder());
            commands.addDefaultDecorator("roomDisclaimer", commands.addRoomDisclaimer());
            commands.addDefaultDecorator(
                "nullTimestampWarning",
                commands.addNullTimestampDisclaimer()
            );
            return bot;
        }

        @commands.newCommand("info", COMMAND_DETAILS.info)
        @commands.alsoInvokedAs(["about"])
        @commands.respondInDirectMessages()
        async botInfo({ respond, userState }: BotMessageContext) {
            respond(TextResponses.botInfo(userState.getCommandPrefix()));
        }

        @commands.newCommand("rooms", COMMAND_DETAILS.rooms)
        @commands.staffOrAllowListOnly()
        @commands.throttlePerCommunity()
        async findRooms(context: BotMessageContext, _arg0: string, ...args: string[]) {
            // Glue parsed arguments back together.
            const searchTerm = args.join(" ");
            const roomsFound = this.getCommunityTracker().findCommunitiesByName(searchTerm);
            // Select only a few communities for display.
            const roomsShown = roomsFound.slice(0, getMaxRooms(isDirectMessage(context.message)));

            const roomCounts = [roomsFound.length, roomsShown.length] as const;

            let message = TextResponses.reportNumberCommunitiesFound(...roomCounts);

            if (roomsShown.length > 0) {
                message += "\n\n";
            }

            message += noindent`
                ${CommunityRooms.showRoomDetails(roomsShown, true)}
                ${TextResponses.offerMoreSearchResults(searchTerm)}
            `;

            await context.respond(`${message}\n`);

            if (roomsFound.length > 0 && isDirectMessage(context.message)) {
                try {
                    const room = roomsFound[0];
                    const serverAddress = CommunityRooms.getRoomJoinLink(room);
                    const serverName = CommunityRooms.getRoomName(room);
                    if (isDirectMessage(context.message)) {
                        context.client.sendOpenGroupInvite(
                            context.author,
                            serverName,
                            serverAddress,
                            1
                        );
                    } else {
                        context.client.sendOpenGroupV3Message(context.room!, undefined, {
                            groupInvitation: {
                                serverName,
                                serverAddress,
                                channelId: 1,
                            },
                        });
                    }
                } catch (err: any) {
                    console.error(
                        `could not send open group invite: ${"message" in err && err.message}`
                    );
                }
            }
        }

        @commands.newCommand("description", COMMAND_DETAILS.description)
        @commands.alsoInvokedAs(["desc", "rules"])
        @commands.staffOrAllowListOnly()
        @commands.throttlePerCommunity(60e3)
        async showRoomDescription({ respond, room }: BotMessageContext) {
            if (room === null) {
                return;
            }
            const roomName = CommunityRooms.getRoomName(room.roomData);
            const description = CommunityRooms.getRoomDescription(room.roomData);
            await respond(TextResponses.quoteRoomDescription(roomName, description));
        }

        @commands.newCommand("challenge", COMMAND_DETAILS.challenge)
        @commands.selfDestructRoomResponse()
        @commands.doNotDecorateWith("donationReminder", "roomDisclaimer")
        async identityChallenge(
            context: BotMessageContext,
            _arg0: string,
            subCommand: string,
            challengeToken: string | undefined
        ) {
            if (subCommand === "start") {
                await this.identityChallengeStart(context);
            } else if (subCommand === "submit" && challengeToken !== undefined) {
                await this.identityChallengeSubmit(context, challengeToken);
            }
        }

        @commands.newCommand("allowlist", COMMAND_DETAILS.allowlist)
        @commands.adminOnly()
        @commands.fromDirectMessagesOnly()
        @commands.requireArguments(2)
        @commands.doNotDecorateWith("defaultThrottling", "donationReminder")
        async allowList(ctx: BotMessageContext, _0: string, subCommand: string, targetId: string) {
            if (subCommand === "add") {
                await this.allowListAdd(ctx, targetId);
            } else if (subCommand === "remove") {
                await this.allowListRemove(ctx, targetId);
            }
        }

        @commands.newCommand("ping", COMMAND_DETAILS.ping)
        @commands.selfDestructRoomResponse()
        @commands.staffOrAllowListOnly()
        @commands.throttlePerUser(10e3)
        @commands.doNotDecorateWith("defaultThrottling", "donationReminder")
        async ping({ respond }: BotMessageContext) {
            respond(TextResponses.pingResponse());
        }

        @commands.newCommand("announce", COMMAND_DETAILS.announce)
        @commands.adminOnly()
        @commands.requireArguments(1)
        @commands.doNotDecorateWith("defaultThrottling", "donationReminder")
        async announceSelf(
            { client, userState }: BotMessageContext,
            _arg0: string,
            targetId: string
        ) {
            const commandPrefix = userState.getCommandPrefix();
            if (isValidUnblindedSessionID(targetId)) {
                await client.send(targetId, TextResponses.botAnnounceMessage(commandPrefix));
            } else {
                console.error(`cannot announce to id: ${targetId}`);
            }
        }

        @commands.newCommand("whatis", COMMAND_DETAILS.whatis)
        @commands.alsoInvokedAs(["w", "what-is"])
        @commands.staffOrAllowListOnly()
        @commands.requireArguments(1)
        @commands.catchCommandExecutionError()
        @commands.throttlePerCommunity()
        async whatIs(context: BotMessageContext, _arg0: string, definitionKey: string) {
            const userDefs = context.userState.getUserDefinitionManager();

            const visibilityOverride = context.userState.isAdminContext(context);

            const communityId = context.room && CommunityRooms.getClientRoomId(context.room);

            const definition = isDirectMessage(context.message)
                ? userDefs.getDefinitionFromDirectMessages(definitionKey, visibilityOverride)
                : userDefs.getDefinitionFromCommunity(communityId!, definitionKey);

            if (!definition) {
                throw new CommandExecutionError("no such definition");
            }

            await context.respond(
                TextResponses.showDefinitionEntries(definition.identifier, definition.entries)
            );
        }

        @commands.newCommand("definition", COMMAND_DETAILS.definition)
        @commands.alsoInvokedAs(["def"])
        @commands.doNotDecorateWith("donationReminder")
        @commands.staffOrAllowListOnly()
        @commands.requireArguments(3)
        @commands.catchCommandExecutionError()
        @commands.selfDestructRoomResponse()
        async definition(
            context: BotMessageContext,
            _arg0: string,
            subCommand: string,
            definitionKey: string,
            ...contentOrNumber: string[]
        ) {
            if (subCommand === "add") {
                const content = contentOrNumber.join(" ");
                await this.addDefinition(context, definitionKey, content);
                await context.respond(`added definition for: '${definitionKey}'`);
            } else if (subCommand === "remove") {
                const number = parseInt(contentOrNumber[0]);
                await this.removeDefinitionEntry(context, definitionKey, number);
                await context.respond(`removed definition #${number} for '${definitionKey}'`);
            }
        }

        @commands.newCommand("defns", COMMAND_DETAILS.definitionNamespace)
        @commands.alsoInvokedAs(["definition-namespace"])
        @commands.doNotDecorateWith("donationReminder")
        @commands.adminOnly()
        @commands.requireArguments(1)
        @commands.catchCommandExecutionError()
        @commands.selfDestructRoomResponse()
        async definitionNamespace(
            context: BotMessageContext,
            _arg0: string,
            subCommand: string,
            namespace?: string,
            ...rest: string[]
        ) {
            const { userState, respond } = context;
            const userDefinitionManager = userState.getUserDefinitionManager();

            const namespaces = [namespace, ...rest].filter(ns => typeof ns !== "undefined");

            if (subCommand === "add" && namespaces.length >= 1) {
                userDefinitionManager.addDefinitionNamespaces(namespaces);
                await respond(
                    pluralize`successfully added ${namespaces.length} definition namespaces`
                );
            } else if (subCommand === "remove" && namespaces.length >= 1) {
                userDefinitionManager.removeDefinitionNamespaces(namespaces);
                await respond(
                    pluralize`successfully removed ${namespaces.length} definition namespaces`
                );
            } else if (subCommand === "size" && namespaces.length >= 1) {
                const maxEntries = UserDefinitionManager.MAX_NAMESPACE_ENTRIES;
                const response = namespaces.map(namespace => {
                    const entries = userDefinitionManager.getNamespaceEntryCount(namespace);
                    return `namespace '${namespace}': ${entries}/${maxEntries}`;
                }).join("\n");
                await respond(response);
            } else if (subCommand === "list") {
                if (typeof namespace === "undefined") {
                    const namespaces = userDefinitionManager.getDefinitionNamespaces();
                    await respond(`namespaces: ${enumerate(namespaces)}`);
                } else {
                    const definitions = userDefinitionManager.getDefinitionsForNamespace(namespace);
                    await respond(`definitions in namespace '${namespace}': ${enumerate(definitions)}`);
                }
            } else if (subCommand === "info" && typeof namespace !== "undefined") {
                const maxEntries = UserDefinitionManager.MAX_NAMESPACE_ENTRIES;
                const config = userDefinitionManager.getDefinitionNamespaceConfig(namespace);
                const entries = userDefinitionManager.getNamespaceEntryCount(namespace);
                await respond(noindent`
                    namespace '${namespace}':
                    ---
                    owners: ${enumerate(config.canBeChangedByCommunities, 'bullets')}
                    readers: ${enumerate(config.canBeReadByCommunities, 'bullets')}
                    public: ${config.canBeReadByAnyone}
                    entries: ${entries}/${maxEntries}
                `);
            } else if (subCommand === "set-public" && typeof namespace !== "undefined" && rest[0] !== undefined) {
                const [publicSetting] = rest;

                if (!["true", "false"].includes(publicSetting)) {
                    return;
                }

                const anyoneCanRead = publicSetting === "true";

                userDefinitionManager.setCanAnyoneReadNamespaces([namespace], anyoneCanRead);

                await respond(`set namespace '${namespace}' to ${anyoneCanRead ? 'public' : 'private'}`);
            }
        }

        @commands.newCommand("defnsc", COMMAND_DETAILS.definitionNamespaceCommunity)
        @commands.doNotDecorateWith("donationReminder")
        @commands.adminOnly()
        @commands.requireArguments(3)
        @commands.catchCommandExecutionError()
        @commands.selfDestructRoomResponse()
        async definitionNamespaceCommunity(
            context: BotMessageContext,
            _arg0: string,
            subCommand: string,
            communityOrDot: string,
            namespace: string,
            newValueOrUndefined?: string
        ) {
            const communityId =
                communityOrDot == "."
                    ? context.room && CommunityRooms.getClientRoomId(context.room)
                    : communityOrDot;

            if (communityId === null) {
                throw new CommandExecutionError("not in community");
            }

            const allowPrivilege =
                newValueOrUndefined === undefined || newValueOrUndefined === "true"
                    ? true
                    : newValueOrUndefined === "false"
                        ? false
                        : null;

            if (allowPrivilege === null) {
                throw new CommandExecutionError("new value must be true or false");
            }

            const userDefinitions = context.userState.getUserDefinitionManager();

            if (subCommand === "read") {
                if (allowPrivilege) {
                    userDefinitions.addCommunityToNamespaceReaders(communityId, [namespace]);
                    await context.respond("added community to namespace readers");
                } else {
                    userDefinitions.removeCommunityFromNamespaceReaders(communityId, [namespace]);
                    await context.respond("removed community from namespace readers");
                }
            } else if (subCommand === "write") {
                if (allowPrivilege) {
                    userDefinitions.addCommunityToNamespaceOwners(communityId, [namespace]);
                    await context.respond("added community to namespace owners");
                } else {
                    userDefinitions.removeCommunityFromNamespaceOwners(communityId, [namespace]);
                    await context.respond("removed community from namespace owners");
                }
            } else if (subCommand === "set-default") {
                userDefinitions.setCommunityDefaultDefinitionNamespace(communityId, namespace);
                await context.respond("set community default namespace");
            }
        }

        @commands.newCommand("link", COMMAND_DETAILS.link)
        @commands.throttlePerCommunity(60e3)
        @commands.staffOrAllowListOnly()
        async postSCOLink({ respond }: BotMessageContext) {
            await respond(TextResponses.communityListingLink());
        }

        @commands.newCommand("donate", COMMAND_DETAILS.donate)
        @commands.staffOrAllowListOnly()
        @commands.throttlePerCommunity(60e3)
        @commands.doNotDecorateWith("donationReminder", "roomDisclaimer")
        async postDonationInfo({ respond }: BotMessageContext) {
            for (const response of TextResponses.donationInfoResponses()) {
                await respond(response);
            }
        }

        // ! Default decorators do not apply here
        @commands.onMissingCommand()
        @commands.staffOrAllowListOnly()
        @commands.selfDestructRoomResponse()
        @commands.throttlePerUser(10e3)
        @commands.addRoomDisclaimer()
        async unknownCommand({ respond, userState, message }: BotMessageContext) {
            const commandPrefix = userState.getCommandPrefix();

            await respond(
                TextResponses.unknownCommandResponse(commandPrefix, !isDirectMessage(message))
            );
        }

        @commands.newCommand("help", COMMAND_DETAILS.help)
        @commands.staffOrAllowListOnly()
        @commands.throttlePerUser(60e3)
        @commands.doNotDecorateWith("defaultThrottling", "donationReminder")
        async helpCommand(context: BotMessageContext, _0: string, opt: string) {
            const { respond, message } = context;
            if (isDirectMessage(message)) {
                this.helpResponse(context, commands, opt);
            } else {
                await respond(TextResponses.helpResponseInCommunity());
            }
        }

        @commands.onMention()
        @commands.respondInDirectMessages()
        @commands.throttlePerUser(60e3)
        async mentionResponse({ respond, userState }: BotMessageContext) {
            const commandPrefix = userState.getCommandPrefix();
            await respond(TextResponses.mentionResponse(commandPrefix));
        }
    }

    return await Bot.createInstance();
}

import type SessionClient from "../../../node-session-client/session-client.js";

import { PrivilegeManager } from "../auth/privilege-manager.js";
import { BotMessageContext } from "../../types/bot-command.js";
import {
    joinCommunityList,
    loadUserState,
    parseSeedWordsFromIdentityOutput,
    savePartialUserState,
} from "./user-state-utils.js";
import { UserDefinitionManager } from "./user-defs.js";
import { SessionOpenGroupV3Server } from "../../types/session-client.js";

/**
 * Class managing the user state of a SessionClient.
 */
export class UserStateManager {
    private constructor(
        private readonly sessionClient: SessionClient,
        private readonly privilegeManager: PrivilegeManager
    ) {}

    private initialState?: UserState;

    private readonly userDefinitionManager: UserDefinitionManager = new UserDefinitionManager(
        () => this.userDefinitionConfig,
        (config: DeepReadonly<UserDefinitionConfig>) => (this.userDefinitionConfig = config)
    );

    public getUserDefinitionManager(): UserDefinitionManager {
        return this.userDefinitionManager;
    }

    private _userDefinitionConfig?: DeepReadonly<UserDefinitionConfig>;

    private get userDefinitionConfig(): DeepReadonly<UserDefinitionConfig> {
        const userDefinitionConfig = this._userDefinitionConfig;

        if (userDefinitionConfig === undefined) {
            throw new Error("User definition config has not been initialized");
        }

        return userDefinitionConfig;
    }

    private set userDefinitionConfig(userDefinitionConfig: DeepReadonly<UserDefinitionConfig>) {
        this._userDefinitionConfig = userDefinitionConfig;
        savePartialUserState({
            userDefinitions: userDefinitionConfig as UserDefinitionConfig, // Fingers crossed :'-)
        });
    }

    /**
     * Initialize and respond to updates to the given session client's state.
     * @param sessionClient Session client to track
     */
    public static async createInstance(
        sessionClient: SessionClient,
        privilegeManager: PrivilegeManager,
        userStateOverrides: Partial<UserState> = {}
    ): Promise<UserStateManager> {
        const userStateManager = new UserStateManager(sessionClient, privilegeManager);

        await userStateManager.initializeIdentity(userStateOverrides);

        return userStateManager;
    }

    private async initializeIdentity(userStateOverrides: Partial<UserState>) {
        await this.loadIdentity(userStateOverrides);
        this.saveGeneratedIdentity(userStateOverrides);
        this.addUserStateHooks(userStateOverrides);
    }

    private async loadIdentity(userStateOverrides: Partial<UserState>) {
        this.initialState = {
            ...loadUserState(),
            ...userStateOverrides,
        };

        const { seed, displayName, lastHash, allowList, communityAutoJoinList, userDefinitions } =
            this.initialState;

        this.userDefinitionConfig = userDefinitions;

        await this.sessionClient.loadIdentity({
            seed: seed ?? undefined,
            displayName,
        });

        if (lastHash !== null) {
            console.log(`Loaded lastHash ${lastHash}`);
            this.sessionClient.lastHash = lastHash;
        }

        if (allowList !== null) {
            this.privilegeManager.setAllowList(allowList);
        }

        await joinCommunityList(this.sessionClient, communityAutoJoinList);
    }

    private saveGeneratedIdentity(userStateOverrides: Partial<UserState>) {
        if ("seed" in userStateOverrides) {
            return;
        }
        // No "keypair to words" transformation available.
        const identityOutput = this.sessionClient.identityOutput;
        if (identityOutput === undefined) {
            return;
        }
        const seed = parseSeedWordsFromIdentityOutput(identityOutput);
        if (seed !== null) {
            savePartialUserState({ seed });
        }
    }

    /**
     * Prints a message with the Session ID assumed by the client, if loaded.
     */
    public echoIdentity() {
        if (this.sessionClient.identityOutput === undefined) {
            throw new Error("Identity not yet loaded");
        }
        const sessionId = this.sessionClient.identityOutput?.match(/05[\da-f]{64}/)?.[0];
        console.log(`Logged in as Session ID ${sessionId}`);
    }

    private addUserStateHooks(userStateOverrides: Partial<UserState>) {
        if ("lastHash" in userStateOverrides) {
            return;
        }
        this.sessionClient.on("updateLastHash", (lastHash: string) => {
            savePartialUserState({ lastHash });
        });
    }

    private saveAllowList() {
        savePartialUserState({
            allowList: this.privilegeManager.getAllowList(),
        });
    }

    public clearAllowList() {
        this.privilegeManager.setAllowList({});
        this.saveAllowList();
    }

    public isStaffOrAllowedContext(context: BotMessageContext) {
        return (
            this.privilegeManager.isStaffOrDMContext(context) ||
            this.privilegeManager.isAllowedContext(context)
        );
    }

    public isAdminContext(context: BotMessageContext) {
        return this.privilegeManager.isAdmin(context.author);
    }

    public addAllowListUser(id: string) {
        this.privilegeManager.addUser(id);
        this.saveAllowList();
    }

    public setAllowListUserBlinding(
        id: string,
        openGroupServer: SessionOpenGroupV3Server,
        blindedId: string
    ) {
        this.privilegeManager.setUserBlinding(id, openGroupServer, blindedId);
        this.saveAllowList();
    }

    public removeAllowListUser(id: string) {
        this.privilegeManager.removeUser(id);
        this.saveAllowList();
    }

    public isAllowedUser(id: string) {
        return this.privilegeManager.isIdInAllowList(id);
    }

    public getCommandPrefix() {
        if (this.initialState === undefined) {
            throw new Error("State not initialized");
        }
        return this.initialState.commandPrefix;
    }

    public getExternalVerificationChallenges() {
        if (this.initialState === undefined) {
            throw new Error("State not initialized");
        }
        return this.initialState.botExternalVerificationChallenges;
    }
}

import { OptionalRewriter } from "../../types/command-decorator.js";
import {
    callbacksForReadonlyUseOfNestedObjectProperty,
    unique,
    useReadonly,
} from "../collections.js";
import { CommandExecutionError } from "../errors.js";

export class UserDefinitionManager {
    constructor(
        private readonly getUserDefinitionConfig: () => DeepReadonly<UserDefinitionConfig>,
        private readonly setUserDefinitionConfig: (
            config: DeepReadonly<UserDefinitionConfig>
        ) => void
    ) {}

    private throwUnknownNamespaceError(namespace: string) {
        throw new CommandExecutionError(
            `namespace does not exist or cannot be read: '${namespace}'`
        );
    }

    private throwNamespaceWriteNotAllowedError(namespace: string) {
        throw new CommandExecutionError(
            `namespace not writeable from this community: '${namespace}'`
        );
    }

    private defaultDefinitionNamespaceConfig(): UserDefinitionNamespaceConfig {
        return {
            canBeChangedByCommunities: [],
            canBeReadByCommunities: [],
            canBeReadByAnyone: false,
        };
    }

    private useDefinitionConfigCallbacks<NestedConfigProperty>() {
        return callbacksForReadonlyUseOfNestedObjectProperty<
            UserDefinitionConfig,
            NestedConfigProperty
        >({
            get: this.getUserDefinitionConfig,
            set: this.setUserDefinitionConfig,
        });
    }

    private useDefinitionNamespaceConfigDict(
        callback: OptionalRewriter<Dict<UserDefinitionNamespaceConfig>>
    ): void {
        useReadonly(
            this.useDefinitionConfigCallbacks<Dict<UserDefinitionNamespaceConfig>>()({
                lastKey: "namespaces",
            }).useWith(callback)
        );
    }

    private useUserDefinitionNamespacesDict(
        callback: OptionalRewriter<UserDefinitionNamespaces>
    ): void {
        useReadonly(
            this.useDefinitionConfigCallbacks<UserDefinitionNamespaces>()({
                lastKey: "definitions",
            }).useWith(callback)
        );
    }

    private useDefinitionNamespaceConfig(
        namespace: string,
        callback: OptionalRewriter<UserDefinitionNamespaceConfig>
    ): void {
        useReadonly(
            this.useDefinitionConfigCallbacks<UserDefinitionNamespaceConfig>()({
                intermediateKeyPath: ["namespaces"],
                lastKey: namespace,
                defaultValue: this.defaultDefinitionNamespaceConfig(),
            }).useWith(callback)
        );
    }

    private useDefinitionCommunityConfig(
        communityId: string,
        callback: OptionalRewriter<UserDefinitionCommunityConfig>
    ): void {
        useReadonly(
            this.useDefinitionConfigCallbacks<UserDefinitionCommunityConfig>()({
                intermediateKeyPath: ["communities"],
                lastKey: communityId,
                defaultValue: { defaultNamespace: "global" },
            }).useWith(callback)
        );
    }

    private useDefinitionNamespace(
        namespace: string,
        callback: OptionalRewriter<Dict<UserDefinitionType>>
    ): void {
        useReadonly(
            this.useDefinitionConfigCallbacks<Dict<UserDefinitionType>>()({
                intermediateKeyPath: ["definitions"],
                lastKey: namespace,
            }).useWith(callback)
        );
    }

    private useDefinition(
        namespace: string,
        identifier: string,
        callback: OptionalRewriter<UserDefinitionType>
    ): void {
        this.assertNamespacesExist([namespace]);
        useReadonly(
            this.useDefinitionConfigCallbacks<UserDefinitionType>()({
                intermediateKeyPath: ["definitions", namespace],
                lastKey: identifier,
                defaultValue: { entries: [] },
            }).useWith(callback)
        );
    }

    private assertNamespacesExist(namespaces: string[]) {
        const config = this.getUserDefinitionConfig();
        for (const namespace of namespaces) {
            if (!Object.hasOwn(config.namespaces, namespace)) {
                this.throwUnknownNamespaceError(namespace);
            }
            if (!Object.hasOwn(config.definitions, namespace)) {
                this.throwUnknownNamespaceError(namespace);
            }
        }
    }

    private assertDefinitionExists(namespace: string, identifier: string) {
        if (!this.getUserDefinitionConfig().definitions?.[namespace]?.[identifier]) {
            throw new CommandExecutionError("no such definition");
        }
    }

    private assertCommunityHasDefaultNamespace(communityId: string) {
        if (!Object.hasOwn(this.getUserDefinitionConfig().communities, communityId)) {
            throw new CommandExecutionError(
                `please specify namespace explicitly, community has no default definition namespace`
            );
        }
    }

    public getDefinitionNamespaces(): string[] {
        return Object.getOwnPropertyNames(this.getUserDefinitionConfig().namespaces);
    }

    public getDefinitionsForNamespace(namespace: string): string[] {
        this.assertNamespacesExist([namespace]);
        return Object.getOwnPropertyNames(this.getUserDefinitionConfig().definitions[namespace]);
    }

    public getDefinitionNamespaceConfig(namespace: string) {
        this.assertNamespacesExist([namespace]);
        return structuredClone(
            this.getUserDefinitionConfig().namespaces[namespace]
        ) as UserDefinitionNamespaceConfig;
    }

    public addDefinitionNamespaces(namespaces: string[]) {
        for (const namespace of namespaces) {
            if (Object.hasOwn(this.getUserDefinitionConfig().namespaces, namespace)) {
                throw new CommandExecutionError(`namespace already exists: ${namespace}`);
            }
        }
        this.useDefinitionNamespaceConfigDict((config) => {
            for (const namespace of namespaces) {
                config[namespace] = this.defaultDefinitionNamespaceConfig();
            }
        });
        this.useUserDefinitionNamespacesDict((namespaceDict) => {
            for (const namespace of namespaces) {
                namespaceDict[namespace] = {};
            }
        });
    }

    public removeDefinitionNamespaces(namespaces: string[]) {
        this.assertNamespacesExist(namespaces);
        this.useDefinitionNamespaceConfigDict((config) => {
            for (const namespace of namespaces) {
                delete config[namespace];
            }
        });
        this.useUserDefinitionNamespacesDict((namespaceDict) => {
            for (const namespace of namespaces) {
                delete namespaceDict[namespace];
            }
        });
    }

    public setCommunityDefaultDefinitionNamespace(communityId: string, namespace: string) {
        this.assertNamespacesExist([namespace]);
        this.useDefinitionCommunityConfig(communityId, (config) => {
            config.defaultNamespace = namespace;
        });
    }

    public setCanAnyoneReadNamespaces(namespaces: string[], newValue: boolean) {
        this.assertNamespacesExist(namespaces);
        this.useDefinitionNamespaceConfigDict((config) => {
            for (const namespace of namespaces) {
                const namespaceConfig = config[namespace];
                namespaceConfig.canBeReadByAnyone = newValue;
            }
        });
    }

    public addCommunityToNamespaceReaders(communityId: string, namespaces: string[]) {
        this.assertNamespacesExist(namespaces);
        this.useDefinitionNamespaceConfigDict((config) => {
            for (const namespace of namespaces) {
                const namespaceConfig = config[namespace];
                namespaceConfig.canBeReadByCommunities = unique([
                    ...namespaceConfig.canBeReadByCommunities,
                    communityId,
                ]);
            }
        });
    }

    public removeCommunityFromNamespaceReaders(communityId: string, namespaces: string[]) {
        this.assertNamespacesExist(namespaces);
        this.removeCommunityFromNamespaceOwners(communityId, namespaces);
        this.useDefinitionNamespaceConfigDict((config) => {
            for (const namespace of namespaces) {
                const namespaceConfig = config[namespace];
                namespaceConfig.canBeReadByCommunities =
                    namespaceConfig.canBeReadByCommunities.filter(
                        (reader) => reader != communityId
                    );
            }
        });
    }

    public addCommunityToNamespaceOwners(communityId: string, namespaces: string[]) {
        this.assertNamespacesExist(namespaces);
        this.addCommunityToNamespaceReaders(communityId, namespaces);
        this.useDefinitionNamespaceConfigDict((config) => {
            for (const namespace of namespaces) {
                const namespaceConfig = config[namespace];
                if (typeof namespaceConfig === "undefined") {
                    throw new CommandExecutionError(`no such namespace: ${namespace}`);
                }
                namespaceConfig.canBeChangedByCommunities = unique([
                    ...namespaceConfig.canBeChangedByCommunities,
                    communityId,
                ]);
            }
        });
    }

    public removeCommunityFromNamespaceOwners(communityId: string, namespaces: string[]) {
        this.assertNamespacesExist(namespaces);
        this.useDefinitionNamespaceConfigDict((config) => {
            for (const namespace of namespaces) {
                const namespaceConfig = config[namespace];
                if (typeof namespaceConfig === "undefined") {
                    throw new CommandExecutionError(`no such namespace: ${namespace}`);
                }
                namespaceConfig.canBeChangedByCommunities =
                    namespaceConfig.canBeChangedByCommunities.filter(
                        (reader) => reader != communityId
                    );
            }
        });
    }

    private static readonly NAMESPACE_SEPARATOR = "/";

    private static makeDefinitionKey(namespace: string, identifier: string) {
        return `${namespace}${UserDefinitionManager.NAMESPACE_SEPARATOR}${identifier}`;
    }

    private static tryParseDefinitionKey(definitionKey: string) {
        const parts = definitionKey.split(UserDefinitionManager.NAMESPACE_SEPARATOR);

        if (parts.length != 2) {
            return null;
        }

        const [namespace, identifier] = parts;

        return {
            namespace,
            identifier,
        };
    }

    public static parseDefinitionKey(definitionKey: string) {
        const parsedDefinitionKey = this.tryParseDefinitionKey(definitionKey);
        if (!parsedDefinitionKey) {
            throw new CommandExecutionError("wrong key format");
        }
        return parsedDefinitionKey;
    }

    private getDefinition(
        namespace: string,
        identifier: string
    ): DeepReadonly<UserDefinitionType> | undefined {
        return this.getUserDefinitionConfig().definitions?.[namespace]?.[identifier];
    }

    private getDefinitionByKey(
        definitionKey: string
    ): DeepReadonly<UserDefinitionType> | undefined {
        const parsedDefinitionKey = UserDefinitionManager.tryParseDefinitionKey(definitionKey);
        if (!parsedDefinitionKey) {
            throw new Error("Could not parse definition key");
        }
        const { namespace, identifier } = parsedDefinitionKey;
        return this.getDefinition(namespace, identifier);
    }

    private canCommunityReadNamespace(communityId: string, namespace: string) {
        this.assertNamespacesExist([namespace]);

        const namespaceConfig = this.getUserDefinitionConfig().namespaces[namespace];

        return namespaceConfig.canBeReadByCommunities.includes(communityId);
    }

    private assertCommunityCanReadNamespace(communityId: string, namespace: string) {
        if (!this.canCommunityReadNamespace(communityId, namespace)
            && !this.getUserDefinitionConfig().namespaces[namespace].canBeReadByAnyone) {
            this.throwUnknownNamespaceError(namespace);
        }
    }

    private assertCommunityCanWriteToNamespace(communityId: string, namespace: string) {
        this.assertNamespacesExist([namespace]);

        const namespaceConfig = this.getUserDefinitionConfig().namespaces[namespace];

        if (!namespaceConfig.canBeChangedByCommunities.includes(communityId)) {
            this.throwNamespaceWriteNotAllowedError(namespace);
        }
    }

    private assertAnyoneCanReadNamespace(namespace: string, visibilityOverride: boolean) {
        this.assertNamespacesExist([namespace]);

        const namespaceConfig = this.getUserDefinitionConfig().namespaces[namespace];

        if (!visibilityOverride && !namespaceConfig.canBeReadByAnyone) {
            this.throwUnknownNamespaceError(namespace);
        }
    }

    private resolveIdentifierOrKeyFromCommunity(communityId: string, identifierOrKey: string) {
        const config = this.getUserDefinitionConfig();

        const maybeParsedDefinitionKey =
            UserDefinitionManager.tryParseDefinitionKey(identifierOrKey);

        if (!maybeParsedDefinitionKey) {
            this.assertCommunityHasDefaultNamespace(communityId);
        }

        const namespace =
            maybeParsedDefinitionKey?.namespace ?? config.communities[communityId].defaultNamespace;

        const identifier = maybeParsedDefinitionKey?.identifier ?? identifierOrKey;

        return {
            namespace,
            identifier,
        };
    }

    public static readonly MAX_NAMESPACE_ENTRIES = 128;
    public static readonly MAX_ENTRY_SIZE = 128;

    public getNamespaceEntryCount(namespace: string) {
        this.assertNamespacesExist([namespace]);
        const namespaceDefinitions = this.getUserDefinitionConfig().definitions[namespace];
        let count = 0;
        for (const definitionIdentifier in namespaceDefinitions) {
            count += namespaceDefinitions[definitionIdentifier].entries.length;
        }
        return count;
    }

    private assertNamespaceHasCapacity(namespace: string) {
        if (this.getNamespaceEntryCount(namespace) >= UserDefinitionManager.MAX_NAMESPACE_ENTRIES) {
            throw new CommandExecutionError(
                `no more room for definitions in namespace '${namespace}'`
            );
        }
    }

    public getDefinitionFromCommunity(
        communityId: string,
        identifierOrKey: string
    ): (DeepReadonly<UserDefinitionType> & { namespace: string; identifier: string }) | undefined {
        const { namespace, identifier } = this.resolveIdentifierOrKeyFromCommunity(
            communityId,
            identifierOrKey
        );

        this.assertCommunityCanReadNamespace(communityId, namespace);

        const definition = this.getDefinition(namespace, identifier);

        return definition && { ...definition, namespace, identifier };
    }

    public addDefinitionFromCommunity(
        communityId: string,
        identifierOrKey: string,
        content: string
    ) {
        const { namespace, identifier } = this.resolveIdentifierOrKeyFromCommunity(
            communityId,
            identifierOrKey
        );

        this.assertCommunityCanWriteToNamespace(communityId, namespace);

        this.assertNamespaceHasCapacity(namespace);

        this.addDefinition(namespace, identifier, content);
    }

    public removeDefinitionEntryFromCommunity(
        communityId: string,
        identifierOrKey: string,
        number: number
    ) {
        const { namespace, identifier } = this.resolveIdentifierOrKeyFromCommunity(
            communityId,
            identifierOrKey
        );

        this.assertCommunityCanWriteToNamespace(communityId, namespace);

        this.removeDefinitionEntry(namespace, identifier, number);
    }

    public getDefinitionFromDirectMessages(
        definitionKey: string,
        visibilityOverride: boolean
    ): (DeepReadonly<UserDefinitionType> & { namespace: string; identifier: string }) | undefined {
        const { namespace, identifier } = UserDefinitionManager.parseDefinitionKey(definitionKey);

        this.assertAnyoneCanReadNamespace(namespace, visibilityOverride);

        const definition = this.getDefinition(namespace, identifier);

        return definition && { ...definition, namespace, identifier };
    }

    public addDefinitionFromPrivilegedDirectMessages(definitionKey: string, content: string) {
        const { namespace, identifier } = UserDefinitionManager.parseDefinitionKey(definitionKey);

        this.assertNamespaceHasCapacity(namespace);

        this.addDefinition(namespace, identifier, content);
    }

    public removeDefinitionFromPrivilegedDirectMessages(definitionKey: string, number: number) {
        const { namespace, identifier } = UserDefinitionManager.parseDefinitionKey(definitionKey);

        this.removeDefinitionEntry(namespace, identifier, number);
    }

    public getAlternateIdentifiersReadableFromCommunity(communityId: string, identifier: string) {
        const config = this.getUserDefinitionConfig();

        const matchingDefinitionKeys = Object.entries(config.definitions)
            .filter(([namespace]) => this.canCommunityReadNamespace(communityId, namespace))
            .flatMap(([namespace, namespaceDefinitions]) => {
                return namespaceDefinitions[identifier]
                    ? [UserDefinitionManager.makeDefinitionKey(namespace, identifier)]
                    : [];
            });

        return matchingDefinitionKeys;
    }

    private addDefinition(namespace: string, identifier: string, content: string) {
        const maxSize = UserDefinitionManager.MAX_ENTRY_SIZE;

        if (content.length > maxSize) {
            throw new CommandExecutionError(`entry too long: ${maxSize} max)`);
        }

        this.useDefinition(namespace, identifier, (definition) => {
            definition.entries.push(content);
        });
    }

    private removeDefinitionEntry(namespace: string, identifier: string, number: number) {
        if (!(Number.isInteger(number) && number >= 1)) {
            throw new CommandExecutionError(`invalid index`);
        }
        this.assertDefinitionExists(namespace, identifier);
        this.useDefinitionNamespace(namespace, (definitions) => {
            const definition = definitions[identifier];
            const spliced = definition.entries.splice(number - 1, 1);
            if (spliced.length == 0) {
                throw new CommandExecutionError(`no such entry: ${number}`);
            }
            if (definition.entries.length == 0) {
                delete definitions[identifier];
            }
        });
    }
}

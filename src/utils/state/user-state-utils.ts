import type SessionClient from "../../../node-session-client/session-client.js";
import * as fsSync from "node:fs";
import { isValidSeedPhrase } from "../session/credentials.js";
import { wait } from "../promises.js";

/**
 * Directory containing static bot user assets.
 */
const BOT_DIRECTORY = "bot";
/**
 * Directory containing dynamic bot user identity and state.
 */
const STATE_DIRECTORY = "state";

/**
 * Directories containing bot state after startup.
 */
const RUNTIME_DIRECTORIES = [STATE_DIRECTORY];
/**
 * Dictionary of static bot assets.
 */
const BOT_FILES = {
    CONFIGURATION: "conf.json",
};
/**
 * Dictionary of dynamic bot assets
 */
const STATE_FILES = {
    SEED: "seed",
    LAST_INBOX_HASH: "last-hash",
    ALLOWLIST: "allowlist.json",
    USER_DEFS: "user-defs.json",
};
/**
 * Time (in ms) after which attempts to join Communities on the auto-join list are aborted.
 */
const COMMUNITY_JOIN_TIMEOUT = 10000;
/**
 * Create all required runtime directories.
 */
function ensureRuntimeDirectories() {
    for (const directory of RUNTIME_DIRECTORIES) {
        if (!fsSync.existsSync(directory)) {
            fsSync.mkdirSync(directory, {
                mode: 448,
                recursive: true,
            });
        }
    }
}
// TODO: Use null
/**
 * Load the contents a bot state file.
 * @param fileName Name of file containing required bot state.
 * @param base Directory to search for file.
 * @param [defaultContents] Text to use for file if empty.
 * @returns Text contents or "" if file is absent.
 */
function loadStateFile(fileName: string, base: string, defaultContents?: any): string {
    const fileNameFull = `${base}/${fileName}`;

    if (!fsSync.existsSync(fileNameFull)) {
        if (typeof defaultContents === "undefined") {
            return "";
        } else {
            const defaultTextContents =
                typeof defaultContents === "string"
                    ? defaultContents
                    : JSON.stringify(defaultContents);
            fsSync.writeFileSync(fileNameFull, defaultTextContents);
            return defaultTextContents;
        }
    }

    return fsSync.readFileSync(fileNameFull).toString();
}
/**
 * Save the given state to a bot state file.
 * @param fileName Name of file to save bot state to.
 * @param base State directory to save file in.
 * @param contents Text contents to save to file.
 */
function saveStateFile(fileName: string, base: string, contents: string): void {
    const fileNameFull = `${base}/${fileName}`;

    fsSync.writeFileSync(fileNameFull, contents);
}
/**
 * Load complete bot user state from disk.
 */
export function loadUserState(): UserState {
    ensureRuntimeDirectories();
    const seed = loadStateFile(STATE_FILES.SEED, STATE_DIRECTORY).trim();
    const lastHash = loadStateFile(STATE_FILES.LAST_INBOX_HASH, STATE_DIRECTORY);
    const allowListRaw = loadStateFile(STATE_FILES.ALLOWLIST, STATE_DIRECTORY);
    const botConfigRaw = loadStateFile(BOT_FILES.CONFIGURATION, BOT_DIRECTORY);
    const userDefinitionsRaw = loadStateFile(STATE_FILES.USER_DEFS, STATE_DIRECTORY, {
        namespaces: {},
        communities: {},
        definitions: {},
    } as UserDefinitionConfig);
    if (botConfigRaw.trim().length === 0) {
        throw new Error(
            "Missing configuration file, please copy bot/conf.sample.json to bot/conf.json"
        );
    }
    const botConfig: BotConfig = JSON.parse(botConfigRaw);
    const userDefinitions: UserDefinitionConfig = JSON.parse(userDefinitionsRaw);
    return {
        seed: seed === "" ? null : seed,
        lastHash: lastHash === "" ? null : lastHash,
        displayName: botConfig.displayName,
        allowList: allowListRaw.length > 0 ? JSON.parse(allowListRaw) : null,
        communityAutoJoinList: botConfig.communityAutoJoinList ?? [],
        commandPrefix: botConfig.commandPrefix,
        botExternalVerificationChallenges: botConfig.botExternalVerificationChallenges ?? [],
        userDefinitions,
    };
}
/**
 * Save given state properties to disk.
 */
export function savePartialUserState(partialUserState: Partial<UserState>): void {
    ensureRuntimeDirectories();
    if (partialUserState.seed !== undefined && partialUserState.seed !== null) {
        saveStateFile(STATE_FILES.SEED, STATE_DIRECTORY, partialUserState.seed);
    }
    if (partialUserState.lastHash !== undefined && partialUserState.lastHash !== null) {
        saveStateFile(STATE_FILES.LAST_INBOX_HASH, STATE_DIRECTORY, partialUserState.lastHash);
    }
    if (partialUserState.allowList !== undefined && partialUserState.allowList !== null) {
        saveStateFile(
            STATE_FILES.ALLOWLIST,
            STATE_DIRECTORY,
            JSON.stringify(partialUserState.allowList)
        );
    }
    if (
        partialUserState.userDefinitions !== undefined &&
        partialUserState.userDefinitions !== null
    ) {
        saveStateFile(
            STATE_FILES.USER_DEFS,
            STATE_DIRECTORY,
            JSON.stringify(partialUserState.userDefinitions)
        );
    }
}
export function parseSeedWordsFromIdentityOutput(identityOutput: string): string | null {
    const SEED_WORDS_PREFIX = "seed words: ";
    if (identityOutput.includes(SEED_WORDS_PREFIX)) {
        const seedWords = identityOutput.split(SEED_WORDS_PREFIX)?.at(-1)?.trim();
        if (seedWords === undefined || !isValidSeedPhrase(seedWords)) {
            console.error("Phrase parsed from identity output seems to be invalid.");
            return null;
        }
        return seedWords;
    }
    return null;
}
export async function joinCommunityList(
    sessionClient: SessionClient,
    communityList: string[]
): Promise<unknown> {
    return Promise.any([
        wait(COMMUNITY_JOIN_TIMEOUT),
        Promise.all(communityList.map((url) => sessionClient.joinOpenGroupV3(url))),
    ]) as any;
}

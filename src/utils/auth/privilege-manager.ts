import { BotMessageContext } from "../../types/bot-command";
import { SessionOpenGroupV3Server } from "../../types/session-client";

export interface PrivilegeManagerOptions {
    /**
     * Array of privileged Session IDs.
     */
    admins: string[];
}

export class PrivilegeManager {
    private allowList: UserAllowList = {};

    private constructor(private readonly admins: string[]) {}

    public static createInstance({ admins }: PrivilegeManagerOptions) {
        return new PrivilegeManager(Array.from(admins));
    }

    public isStaffOrDMContext(context: BotMessageContext) {
        if (context.room) {
            const { room, author } = context;
            const { admins = [], moderators = [] } = room.roomData;
            const staff = [...admins, ...moderators];
            return staff.includes(author);
        }
        return context.author?.startsWith("05");
    }

    public isAllowedContext(context: BotMessageContext) {
        return this.isIdInAllowList(context.author);
    }

    public setAllowList(allowList: UserAllowList) {
        // TODO: Sanity check
        this.allowList = allowList;
    }

    public getAllowList(): UserAllowList {
        return this.allowList;
    }

    public hasUser(id: string): boolean {
        return Object.hasOwn(this.allowList, id);
    }

    public addUser(id: string) {
        if (this.hasUser(id)) {
            return;
        }
        this.allowList[id] = {};
    }

    public removeUser(id: string) {
        if (!this.hasUser(id)) {
            return;
        }
        delete this.allowList[id];
    }

    public setUserBlinding(
        id: string,
        openGroupServer: SessionOpenGroupV3Server,
        blindedId: string
    ) {
        if (!this.hasUser(id)) {
            throw new Error("User does not exist in allowlist");
        }
        this.allowList[id] = {
            ...this.allowList[id],
            [openGroupServer.serverPubkeyHex]: blindedId,
        };
    }

    public isIdInAllowList(id: string) {
        if (this.hasUser(id)) {
            return true;
        }
        const allowedIDs: string[] = Object.values(this.allowList).flatMap<string>((blindings) =>
            Object.values(blindings)
        );
        return allowedIDs.includes(id);
    }

    public isAdmin(id: string) {
        return this.admins.includes(id);
    }
}

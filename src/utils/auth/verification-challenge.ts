import * as crypto from "crypto";

/**
 * Represents a nonce-based timed challenge to authenticate a blinded Session ID.
 */
export class VerificationChallenge {
    /**
     * Nonce length in bytes.
     */
    private static NONCE_SIZE = 16;
    /**
     * Time in milliseconds for which the challenge is valid.
     */
    private static EXPIRY_TIME = 5 * 60 * 1000;

    /**
     * Millisecond UNIX timestamp of challenge expiry.
     */
    private readonly expires: number;

    /**
     * Challenge nonce as hexadecimal string with {@link NONCE_SIZE} bytes of entropy.
     */
    public readonly nonce: string;

    constructor(public readonly author: string) {
        this.nonce = crypto.randomBytes(VerificationChallenge.NONCE_SIZE).toString("hex");
        this.expires = Date.now() + VerificationChallenge.EXPIRY_TIME;
    }

    /**
     * Checks whether the current challenge is still valid..
     * @returns `true` if the challenge was created more than {@link EXPIRY_TIME} ms ago, `false` otherwise.
     */
    expired(): boolean {
        return Date.now() > this.expires;
    }

    /**
     * Attempts to verify the current challenge against the given token.
     * @param nonce Hexadecimal token to verify against.
     * @returns `true` if the challenge is valid and matches the token, `false` otherwise.
     */
    verify(nonce: string): boolean {
        return !this.expired() && nonce == this.nonce;
    }
}

export class VerificationChallengeManager {
    /**
     * Record of active verification challenges, organized by unblinded ID of target user.
     */
    private verificationChallenges: Map<string, VerificationChallenge> = new Map();

    public addVerificationChallenge(challenge: VerificationChallenge) {
        this.verificationChallenges.set(challenge.nonce, challenge);
    }

    public getVerificationChallenge(nonce: string): VerificationChallenge {
        if (!this.hasVerificationChallenge(nonce)) {
            throw new Error("need to check whether challenge exists first");
        }
        return this.verificationChallenges.get(nonce)!;
    }

    public hasVerificationChallenge(nonce: string) {
        return this.verificationChallenges.has(nonce);
    }

    public deleteVerificationChallenge(nonce: string) {
        if (this.verificationChallenges.delete(nonce)) {
            return true;
        } else {
            console.error("deleting non-existent identity challenge");
            return false;
        }
    }
}

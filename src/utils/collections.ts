import { isDeepStrictEqual } from "util";
import { OptionalRewriter } from "../types/command-decorator";

export function singleton<T>(itemOrItems: T | T[] | undefined): T[] {
    if (Array.isArray(itemOrItems)) {
        return itemOrItems;
    } else if (itemOrItems === undefined) {
        return [];
    } else {
        return [itemOrItems];
    }
}

export function omitEmpty<T extends Object>(obj: Partial<T>): Partial<T> {
    return Object.fromEntries(
        Object.entries(obj).filter((key, val) => val !== null && val !== undefined)
    ) as T;
}

export function bySmallerPropFirst<Item, Prop>(getProp: (item: Item) => Prop) {
    return function (item1: Item, item2: Item) {
        const [prop1, prop2] = [item1, item2].map(getProp);
        return Number(prop1 > prop2) - Number(prop1 < prop2);
    };
}

export function byLargerPropFirst<Item, Prop>(getProp: (item: Item) => Prop) {
    return function (item1: Item, item2: Item) {
        const [prop1, prop2] = [item1, item2].map(getProp);
        return Number(prop1 < prop2) - Number(prop1 > prop2);
    };
}

export function unique<T>(array: Array<T>): Array<T> {
    return [...new Set(array)];
}

export function callbacksForReadonlyUseOfNestedObjectProperty<T, S>(
    accessors: PropertyDescriptor | { set(t: T): void; get(): T }
) {
    return function (args: {
        intermediateKeyPath?: (string | symbol | number)[];
        lastKey: string | symbol | number;
        defaultValue?: S;
    }) {
        const { set: setValue, get: getValue } = accessors;
        const { lastKey } = args;
        const intermediateKeyPath = args?.intermediateKeyPath ?? [];
        const getDefaultValue = (): S => {
            if (typeof args.defaultValue === "undefined") {
                throw new Error("Could not supply default value for subproperty");
            }
            return args.defaultValue;
        };

        if (setValue === undefined || getValue === undefined) {
            throw new Error("Descriptor missing accessors");
        }

        return {
            useWith: (callback: OptionalRewriter<S>): ReadonlyUseCallback<T, S> => ({
                getObject() {
                    return getValue();
                },
                setModified(newValue) {
                    setValue(newValue);
                },
                setOverwritten(newSubProperty) {
                    const cloned = structuredClone(getValue()) as T;
                    let carrier: any = cloned;
                    for (const key of intermediateKeyPath) {
                        carrier = carrier[key];
                    }
                    carrier[lastKey] = newSubProperty;
                    setValue(cloned);
                },
                transform(clonedValue) {
                    let subProperty: any = clonedValue;
                    for (const key of [...intermediateKeyPath, lastKey]) {
                        subProperty = subProperty[key];
                    }
                    subProperty ??= getDefaultValue();
                    return callback(subProperty as S) ?? subProperty;
                },
            }),
        };
    };
}

interface ReadonlyUseCallback<T, S> {
    getObject: () => T;
    setModified: (t: T) => void;
    setOverwritten: (s: S) => void;
    transform: (t: T) => S;
}

export function useReadonly<T, S>(callbacks: ReadonlyUseCallback<T, S>): void {
    const object = callbacks.getObject();
    const writeableObject = structuredClone(object) as T;
    const result = callbacks.transform(writeableObject);
    if (!isDeepStrictEqual(object, writeableObject)) {
        callbacks.setModified(structuredClone(writeableObject));
    } else if (typeof result !== "undefined") {
        callbacks.setOverwritten(result);
    }
}

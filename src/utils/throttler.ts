import { truncate } from "./text";

export class InvocationThrottler {
    private lastInvocations: Map<string, number> = new Map();

    private constructor(private readonly delayMiliseconds: number) {}

    public static withDelay(delayMiliseconds: number): InvocationThrottler {
        return new InvocationThrottler(delayMiliseconds);
    }

    public tryInvokeAs(key: string): boolean {
        const nextInvocation = (this.lastInvocations.get(key) ?? 0) + this.delayMiliseconds;

        const invocationTime = Date.now();

        const keyShort = truncate(key, 16);

        if (nextInvocation > invocationTime) {
            console.log(`throttler: rejecting invocation for ${keyShort}`);
            return false;
        }

        this.lastInvocations.set(key, invocationTime);

        console.log(`throttler: approving invocation for ${keyShort}`);
        return true;
    }
}

import { TextResponses } from "../text/bot/responses";
import { BotMessageContext } from "../types/bot-command";
import { BotCommandRegistry } from "./commands/command-registry";
import { CommunityListTracker } from "./communities/community-list-tracker";
import { isValidUnblindedSessionID } from "./session/credentials";
import { isDirectMessage } from "./session/message-context";
import { VerificationChallenge, VerificationChallengeManager } from "./auth/verification-challenge";
import { CommunityRooms } from "./communities/community-rooms";
import { CommandExecutionError } from "./errors";

export class BaseBot {
    private readonly communityListTracker: CommunityListTracker = new CommunityListTracker();

    private readonly challenges = new VerificationChallengeManager();

    protected getCommunityTracker() {
        return this.communityListTracker;
    }

    protected getChallengeManager() {
        return this.challenges;
    }

    protected async initialize() {
        this.communityListTracker.startFetchingRegularly();
        await this.communityListTracker.fetchData();
    }

    protected async allowListAdd(context: BotMessageContext, userId: string) {
        if (isValidUnblindedSessionID(userId)) {
            context.userState.addAllowListUser(userId);
        }
    }

    async allowListRemove(context: BotMessageContext, userId: string) {
        context.userState.removeAllowListUser(userId);
    }

    async identityChallengeStart({ respond, author, userState, message }: BotMessageContext) {
        if (!isDirectMessage(message)) {
            return;
        }
        const challenge = new VerificationChallenge(author);
        const commandPrefix = userState.getCommandPrefix();
        this.getChallengeManager().addVerificationChallenge(challenge);
        await respond(TextResponses.identityChallengeInstructions(commandPrefix));
        await respond(challenge.nonce);
    }

    async identityChallengeSubmit(
        { respond, message, author, userState, room }: BotMessageContext,
        challengeToken: string
    ) {
        const challenge = this.getChallengeManager().getVerificationChallenge(challengeToken);

        if (challenge === undefined) {
            return;
        }

        // Check follows challenge check to prevent abuse
        if (isDirectMessage(message) || room === null) {
            await respond(TextResponses.identityChallengeWrongSubmissionLocation());
            return;
        }

        if (challenge.verify(challengeToken)) {
            userState.setAllowListUserBlinding(challenge.author, room.server, author);
            await respond(TextResponses.identityChallengeSuccess());
        } else {
            await respond(TextResponses.identityChallengeExpiry());
        }

        this.getChallengeManager().deleteVerificationChallenge(challengeToken);
    }

    async helpResponse(
        { respond, userState }: BotMessageContext,
        commands: BotCommandRegistry,
        arg1?: string
    ) {
        const commandPrefix = userState.getCommandPrefix();

        const showAll = arg1 == "all";
        const commandList =
            showAll || arg1 === undefined ? commands.getAllCommands(showAll) : [arg1];

        await respond("here are some of the commands i know:");
        for (const commandName of commandList) {
            const details = commands.getCommandDetails(commandName);
            if (!details) {
                respond(`no such command: '${commandName}'`);
            }
            if (!details.synopsis) {
                continue;
            }
            respond(TextResponses.formatCommandDetails(details, commandPrefix));
        }
    }

    async addDefinition(context: BotMessageContext, definitionKey: string, content: string) {
        const { respond, userState, message, room } = context;
        const userDefinitionManager = userState.getUserDefinitionManager();

        if (content.length >= 256) {
            throw new CommandExecutionError("entry too long");
        }

        if (isDirectMessage(message)) {
            if (userState.isAdminContext(context)) {
                userDefinitionManager.addDefinitionFromPrivilegedDirectMessages(
                    definitionKey,
                    content
                );
            }
        } else {
            const communityId = CommunityRooms.getClientRoomId(room!);
            userDefinitionManager.addDefinitionFromCommunity(communityId, definitionKey, content);
        }
    }

    async removeDefinitionEntry(context: BotMessageContext, definitionKey: string, number: number) {
        const { respond, userState, message, room } = context;
        const userDefinitionManager = userState.getUserDefinitionManager();

        if (isDirectMessage(message)) {
            if (userState.isAdminContext(context)) {
                userDefinitionManager.removeDefinitionFromPrivilegedDirectMessages(
                    definitionKey,
                    number
                );
            }
        } else {
            const communityId = CommunityRooms.getClientRoomId(room!);
            userDefinitionManager.removeDefinitionEntryFromCommunity(
                communityId,
                definitionKey,
                number
            );
        }
    }
}

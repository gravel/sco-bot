import { SessionMessage } from "../../types/session-message";

export function isDirectMessage(message: SessionMessage) {
    return !("room" in message);
}

import * as mnemonic from "../../../node-session-client/external/mnemonic/mnemonic.js";

/**
 * Checks whether the given phrase is a valid mnemonic seed phrase.
 * @param seedPhrase String of mnemonic words.
 * @returns True if phrase is valid, false otherwise.
 */
export function isValidSeedPhrase(seedPhrase: string): boolean {
    try {
        mnemonic.mn_decode(seedPhrase);
    } catch (error) {
        // Passthrough unrelated errors.
        // (MnemonicError is not exported.)
        if (!(error instanceof Error) || error.constructor.name !== "MnemonicError") {
            throw error;
        }

        return false;
    }
    return true;
}

export function isValidUnblindedSessionID(userId: string) {
    return userId.match(/^05[0-9a-f]{64}$/i) !== null;
}

import { BotMessageContext, BotOpaque, MessageHandler } from "../../types/bot-command";
import {
    contextFilter,
    editCommandAsDecorator,
    editContextAsDecorator,
} from "./command-decorator-tools";
import { isDirectMessage } from "../session/message-context";
import { InvocationThrottler } from "../throttler";
import { TextResponses } from "../../text/bot/responses";
import { CommandExecutionError } from "../errors";
import { DAY_MS } from "../numeric";

/**
 * Schedules the deletion of an open group message.
 * @param context Context message was sent under.
 * @param messageId Message ID of message to delete.
 * @param delayMiliseconds Delay after which to delete message.
 * Does not wait for message to be received.
 */
function scheduleOpenGroupV3MessageDeletion(
    context: BotMessageContext,
    messageId: number,
    delayMiliseconds: number
) {
    console.log(`Deleting message ${messageId} in ${delayMiliseconds} ms`);
    setTimeout(async () => {
        try {
            await context.client.deleteOpenGroupV3Message(context.room, [messageId]);
        } catch (error) {
            console.error("Failed to auto-destruct message", error);
        }
    }, delayMiliseconds as number);
}

export class CommandDecorators {
    /**
     * Auto-destructs all messages sent by the decorated command in a Community.
     * Applies only to messages sent using `context.respond`.
     * @param delayMiliseconds Time in milliseconds to wait before deletion.
     */
    public selfDestructRoomResponse = (delayMiliseconds = 15e3) =>
        editContextAsDecorator((context: BotMessageContext) => ({
            ...context,
            async respond(message: string, options: any = {}) {
                const messageId = await context.respond(message, options);
                if (context.room !== undefined && typeof messageId === "number") {
                    scheduleOpenGroupV3MessageDeletion(context, messageId, delayMiliseconds);
                }
                return messageId;
            },
        }));

    public addRoomDisclaimer = () =>
        editContextAsDecorator((context: BotMessageContext) => ({
            ...context,
            async respond(message: string, options: any = {}) {
                if (isDirectMessage(context.message)) {
                    return await context.respond(message, options);
                } else {
                    return await context.respond(
                        `${message}\n(in communities, i respond to staff and allowlist only)`,
                        options
                    );
                }
            },
        }));

    public addDonationReminder = () =>
        editContextAsDecorator((context: BotMessageContext) => ({
            ...context,
            async respond(message: string, options: any = {}) {
                if (message.startsWith("error")) {
                    return await context.respond(message, options);
                }
                return await context.respond(
                    `${message}${TextResponses.donationReminder(
                        context.userState.getCommandPrefix(),
                        !isDirectMessage(context.message)
                    )}`,
                    options
                );
            },
        }));

    /**
     * Limits the reception of the decorated command to direct messages.
     */
    public fromDirectMessagesOnly = () =>
        editCommandAsDecorator(
            (originalCommand: MessageHandler) =>
                async function command(
                    this: BotOpaque,
                    context: BotMessageContext,
                    ...args: string[]
                ) {
                    if (isDirectMessage(context.message)) {
                        return await originalCommand.call(this, context, ...args);
                    }
                }
        );

    /**
     * Blocks the decorated command in public rooms,
     * unless invoked by room staff or allow-listed users.
     */
    public staffOrAllowListOnly = () =>
        contextFilter(
            (context: BotMessageContext) => context.userState.isStaffOrAllowedContext(context),
            async ({ author }: BotMessageContext) =>
                console.log(`Denied execution of command to ${author}`)
        );

    /**
     * Blocks the decorated command unless invoked by an admin.
     */
    public adminOnly = () =>
        contextFilter(
            (context: BotMessageContext) => context.userState.isAdminContext(context),
            async ({ author }: BotMessageContext) =>
                console.log(`Denied execution of admin command to ${author}`)
        );

    /**
     * Redirects all text responses from command to author.
     */
    public respondInDirectMessages = () =>
        editContextAsDecorator((context: BotMessageContext) => ({
            ...context,
            async respond(message: string, options: any) {
                const { author, client } = context;
                if (author.startsWith("05")) {
                    return await client.send(author, message, options);
                } else {
                    console.log("Cannot respond to blinded ID in direct messages");
                }
            },
        }));

    /**
     * Modifies the decorated command to ignore repeated invocations in the same community.
     * @param delayMiliseconds Period since last invocation considered a repeat.
     */
    public throttlePerCommunity(delayMiliseconds = 10e3) {
        const lastInvocations = InvocationThrottler.withDelay(delayMiliseconds);

        return contextFilter(({ room }: BotMessageContext) => {
            if (!room) {
                return true;
            }

            const roomId = `${room.token}+${room.server.serverPubkeyHex}`;

            return lastInvocations.tryInvokeAs(roomId);
        });
    }

    /**
     * Modifies the decorated command to ignore repeated invocations from the same user.
     * @param delayMiliseconds Period since last invocation considered a repeat.
     */
    public throttlePerUser(delayMiliseconds = 10e3) {
        const lastInvocations = InvocationThrottler.withDelay(delayMiliseconds);

        return contextFilter(({ author }: BotMessageContext) => {
            return lastInvocations.tryInvokeAs(author);
        });
    }

    public requireArguments(numArgs: number) {
        return editCommandAsDecorator((originalCommand) => {
            return async function command(
                this: BotOpaque,
                context: BotMessageContext,
                arg0: string,
                ...args: string[]
            ) {
                if (args.length >= numArgs) {
                    return await originalCommand.call(this, context, arg0, ...args);
                }
            };
        });
    }

    public catchCommandExecutionError(suppressErrorResponse = false) {
        return editCommandAsDecorator((originalCommand) => {
            return async function command(
                this: BotOpaque,
                context: BotMessageContext,
                ...args: string[]
            ) {
                try {
                    return await originalCommand.call(this, context, ...args);
                } catch (err) {
                    if (err instanceof CommandExecutionError) {
                        if (!suppressErrorResponse) {
                            const messageId = await context.respond(`error: ${err.message}`);
                            if (context.room !== undefined && typeof messageId === "number") {
                                scheduleOpenGroupV3MessageDeletion(context, messageId, 10e3);
                            }
                        }
                        console.error(err);
                    } else {
                        throw err;
                    }
                }
            };
        });
    }

    public addNullTimestampDisclaimer = () => {
        const throttler = this.throttlePerUser(DAY_MS);
        const throttledDisclaimer = throttler(
            async (context: BotMessageContext) =>
                void (await context.respond(TextResponses.nullTimestampWarning())),
            {} as ClassMethodDecoratorContext
        );

        return editCommandAsDecorator((originalCommand) => {
            return async function (this: BotOpaque, context: BotMessageContext, ...args: string[]) {
                await originalCommand.call(this, context, ...args);
                if (context.message.timestamp === null && isDirectMessage(context.message)) {
                    throttledDisclaimer(context);
                }
            };
        });
    };
}

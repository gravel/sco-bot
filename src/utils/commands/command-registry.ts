import type SessionClient from "../../../node-session-client/session-client";
import {
    BotCommandDetails,
    BotCommandProps,
    BotFactory,
    BotMessageContext,
    BotOpaque,
    CommandHandler,
    MessageHandler,
    TextMessageHandler,
} from "../../types/bot-command";
import { BotCommandDecorator, DefaultDecoratorId } from "../../types/command-decorator";
import { UserStateManager } from "../state/user-state";
import { editCommandAsDecorator } from "./command-decorator-tools";
import { CommandDecorators } from "./command-decorators";

/**
 * Represents a collection of plain and specialized chat commands.
 */
export class BotCommandRegistry extends CommandDecorators {
    /**
     * Bot instance hosting commands and carrying state.
     */
    private bot: BotOpaque;

    private constructor(private readonly sessionClient: SessionClient) {
        super();
    }

    public static async createInstance(
        sessionClient: SessionClient,
        makeBot: BotFactory,
        userStateManager: UserStateManager
    ): Promise<BotCommandRegistry> {
        const botCommandController = new BotCommandRegistry(sessionClient);
        botCommandController.bot = await makeBot(botCommandController);
        botCommandController.addSimpleTextCommands(
            ...userStateManager.getExternalVerificationChallenges()
        );
        return botCommandController;
    }

    private currentCommandName: string | undefined;

    private readonly commands: Record<string, CommandHandler> = {};

    private readonly commandDetails: Record<string, Readonly<BotCommandDetails>> = {};

    private readonly commandProps: Record<string, BotCommandProps> = {};

    private readonly textCommands: Record<string, TextMessageHandler> = {};

    private readonly defaultCommandDecorators: {
        id: DefaultDecoratorId;
        decorator: BotCommandDecorator;
    }[] = [];

    public getAllCommands(showUnlisted = false): string[] {
        return Object.getOwnPropertyNames(this.commandDetails).filter(
            (command) => showUnlisted || !this.commandDetails[command].unlisted
        );
    }

    public getCurrentCommandName(): string {
        const name = this.currentCommandName;
        if (name === undefined) {
            throw new Error("No command being currently edited");
        }
        return name;
    }

    private useCommandProps(commandName: string) {
        return (this.commandProps[commandName] ??= {});
    }

    private getDecoratedCommand(commandName: string): CommandHandler {
        const decoratorExceptions =
            this.useCommandProps(commandName).defaultDecoratorExceptions ?? [];

        const decorators = this.defaultCommandDecorators.filter(
            ({ id }) => !decoratorExceptions.includes(id)
        );

        let command = this.commands[commandName];

        for (const { decorator } of decorators) {
            command = decorator(command, {
                kind: "method",
                name: commandName,
                private: false,
                static: false,
            } as ClassMethodDecoratorContext<BotCommandRegistry, CommandHandler> & { static: false });
        }

        if (!command) {
            throw new Error("Got undefined command after decoration");
        }

        return command;
    }

    public getCommandDetails(commandName: string): Readonly<BotCommandDetails> {
        return this.commandDetails[commandName];
    }

    private missingCommandResponse: CommandHandler | null = null;

    private mentionResponse: TextMessageHandler | null = null;

    private invokeRunner(
        runner: CommandHandler | null,
        context: BotMessageContext,
        commandArguments: string[]
    ) {
        runner?.call(this.bot, context, ...commandArguments);
    }

    private invokeMessageResponder(
        responder: TextMessageHandler | null,
        context: BotMessageContext
    ) {
        responder?.call(this.bot, context);
    }

    public hasMissingCommandResponse() {
        return this.missingCommandResponse != null;
    }

    public hasCommand(commandName: string) {
        return Object.hasOwn(this.commands, commandName);
    }

    public hasTextCommand(text: string) {
        return Object.hasOwn(this.textCommands, text);
    }

    public runMissingCommandResponse(context: BotMessageContext, ...commandArguments: string[]) {
        this.invokeRunner(this.missingCommandResponse, context, commandArguments);
    }

    public runCommand(
        commandName: string,
        commandContext: BotMessageContext,
        commandArguments: string[]
    ) {
        return this.invokeRunner(
            this.getDecoratedCommand(commandName),
            commandContext,
            commandArguments
        );
    }

    public runTextCommand(textCommand: string, commandContext: BotMessageContext) {
        return this.invokeMessageResponder(this.textCommands[textCommand], commandContext);
    }

    public runDefaultTextResponse(context: BotMessageContext) {
        this.invokeMessageResponder(this.mentionResponse, context);
    }

    public addDefaultDecorator(id: DefaultDecoratorId, decorator: BotCommandDecorator) {
        this.defaultCommandDecorators.push({ id, decorator });
    }

    public addSimpleTextCommands(...textCommands: SimpleTextCommand[]) {
        for (const { commandText, responseText } of textCommands) {
            if (this.hasTextCommand(commandText)) {
                throw new Error(`Text command '${commandText}' already exists`);
            }

            this.textCommands[commandText] = async function (context: BotMessageContext) {
                await context.respond(responseText);
            };
        }
    }

    /**
     * Registers the decorated command under the given name.
     * Must be applied after all command modifications (written first).
     * @param commandName Name used to invoke command.
     * @param details Command documentation.
     */
    public newCommand(
        commandName: string,
        details: BotCommandDetails = {},
        props: BotCommandProps = {}
    ) {
        this.currentCommandName = commandName;
        this.commandDetails[commandName] = details;
        this.commandProps[commandName] = props;

        return this.alsoInvokedAs([commandName]);
    }

    /**
     * Registers the decorated command under the given aliases.
     * Must be applied after all command modifications.
     */
    public alsoInvokedAs(commandAliasNames: string[]) {
        const currentCommandName = this.getCurrentCommandName();
        return editCommandAsDecorator((command: MessageHandler) => {
            for (const name of commandAliasNames) {
                if (this.hasCommand(name)) {
                    throw new Error(`Command '${name}' already exists`);
                }
            }

            const props = this.commandProps[currentCommandName];

            for (const name of commandAliasNames) {
                this.commands[name] = command;
                this.commandProps[name] = props;
            }

            return command;
        });
    }

    /**
     * Registers the decorated command to execute when an invalid command is invoked.
     */
    public onMissingCommand() {
        return editCommandAsDecorator((command: MessageHandler) => {
            if (this.missingCommandResponse !== null) {
                throw new Error("Duplicate missing command response");
            }

            this.missingCommandResponse = command;

            return command;
        });
    }

    /**
     * Registers the decorated responded to execute when the bot is addressed in non-command form.
     */
    public onMention() {
        return editCommandAsDecorator((responder: MessageHandler) => {
            if (this.mentionResponse !== null) {
                throw new Error("Duplicate mention response");
            }

            this.mentionResponse = responder;

            return responder;
        });
    }

    public doNotDecorateWith(...ids: DefaultDecoratorId[]) {
        const commandName = this.getCurrentCommandName();
        return editCommandAsDecorator((handler: MessageHandler) => {
            const commandProps = this.useCommandProps(commandName);
            commandProps.defaultDecoratorExceptions ??= [];
            commandProps.defaultDecoratorExceptions!.push(...ids);

            return handler;
        });
    }
}

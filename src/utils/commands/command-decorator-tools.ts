import {
    BotMessageContext,
    BotOpaque,
    CommandHandler,
    MessageHandler,
} from "../../types/bot-command";
import { Predicate, Rewriter } from "../../types/command-decorator";

export function editCommandAsDecorator(wrapHandler: Rewriter<MessageHandler>) {
    return function commandDecorator(
        messageHandler: MessageHandler,
        _: ClassMethodDecoratorContext
    ): MessageHandler {
        return wrapHandler(messageHandler);
    };
}

export function editContextAsDecorator(wrapContext: Rewriter<BotMessageContext>) {
    return editCommandAsDecorator(
        (originalCommand: CommandHandler) =>
            async function (this: BotOpaque, context: BotMessageContext, ...args: string[]) {
                return await originalCommand.call(this, wrapContext(context), ...args);
            }
    );
}

export function contextFilter(
    allowContext: Predicate<BotMessageContext>,
    onFailure?: MessageHandler
) {
    return editCommandAsDecorator(
        (originalCommand: CommandHandler) =>
            async function (this: BotOpaque, context: BotMessageContext, ...args: string[]) {
                if (allowContext(context)) {
                    return await originalCommand.call(this, context, ...args);
                } else {
                    return await onFailure?.call(this, context, ...args);
                }
            }
    );
}

// TODO: Provide context to rewriter
export function editResponseAsDecorator(replaceResponse: Rewriter<BotMessageContext["respond"]>) {
    return editContextAsDecorator((context: BotMessageContext) => ({
        ...context,
        respond: replaceResponse(context.respond),
    }));
}

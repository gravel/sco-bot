import { trimAffix } from "../text";

/**
 * Utility class to parse commands.
 */
export class CommandStringParser {
    /**
     * Parses a chat command into words, respecting quotes.
     * @param commandString Raw command string, excluding prefix.
     * @returns Array of command name and parameters.
     */
    public static parseCommandString(commandString: string): string[] | null {
        const words = commandString.split(/\s+/g);
        const allQuotes: Array<QuoteRangeInclusive> | null = this.getAllCommandQuoteRanges(words);
        if (allQuotes === null) {
            return null;
        }
        this.spliceAllCommandQuoteRanges(words, allQuotes);

        return words;
    }

    private static readonly QUOTE_CANDIDATES = ["'", '"'];

    private static getAllCommandQuoteRanges(
        commandWords: string[]
    ): Array<QuoteRangeInclusive> | null {
        const allQuoteRanges: Array<QuoteRangeInclusive> = [];
        let currentQuoteRange: { from: number; quoteCharacter: string } | null = null;

        // Start at 1 to preserve command name argument.
        nextWord: for (let wordIndex = 1; wordIndex < commandWords.length; wordIndex += 1) {
            const word = commandWords[wordIndex];
            if (currentQuoteRange === null) {
                for (const quoteCandidate of this.QUOTE_CANDIDATES) {
                    if (word.startsWith(quoteCandidate) && word.endsWith(quoteCandidate)) {
                        commandWords[wordIndex] = trimAffix(word, quoteCandidate, "both");
                        continue nextWord;
                    } else if (word.startsWith(quoteCandidate)) {
                        currentQuoteRange = { from: wordIndex, quoteCharacter: quoteCandidate };
                        continue nextWord;
                    }
                }
            } else {
                const { from, quoteCharacter } = currentQuoteRange;
                if (word.endsWith(quoteCharacter)) {
                    allQuoteRanges.push({ from, to: wordIndex, quoteCharacter });
                    currentQuoteRange = null;
                }
            }
        }

        if (currentQuoteRange !== null) {
            return null;
        }

        return allQuoteRanges;
    }

    private static concatQuoteRange(
        commandWords: string[],
        { from, to, quoteCharacter }: QuoteRangeInclusive
    ) {
        const rangeWords = commandWords.slice(from, to + 1);

        if (rangeWords.length == 0) {
            return "";
        }

        rangeWords[0] = trimAffix(rangeWords[0], quoteCharacter, "prefix");
        rangeWords[rangeWords.length - 1] = trimAffix(rangeWords.at(-1)!, quoteCharacter, "suffix");

        return rangeWords.join(" ");
    }

    private static spliceAllCommandQuoteRanges(
        commandWords: string[],
        quoteRanges: Array<QuoteRangeInclusive>
    ) {
        const quoteRangesReversed = Array.from(quoteRanges).reverse();
        for (const quoteRange of quoteRangesReversed) {
            const { from, to } = quoteRange;
            commandWords.splice(
                from,
                to - from + 1,
                this.concatQuoteRange(commandWords, quoteRange)
            );
        }
    }
}

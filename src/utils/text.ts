/**
 * Remove a prefix, suffix, or both from a string.
 * @param target String to trim.
 * @param affix Affix to trim from the given sides.
 * @param type The affix type to remove.
 * @param lenient Do not throw when the given affixes are not present.
 * @returns Trimmed string.
 */
export function trimAffix(
    target: string,
    affix: string,
    type: "prefix" | "suffix" | "both",
    lenient = false
) {
    let result = target;

    if (type !== "suffix") {
        if (!lenient && !result.startsWith(affix)) {
            throw new Error("Prefix not found in string");
        }
        result = result.substring(affix.length);
    }

    if (type !== "prefix") {
        if (!lenient && !result.endsWith(affix)) {
            throw new Error("Suffix not found in string");
        }
        result = result.substring(0, -affix.length);
    }

    return result;
}

/**
 * Template tag. Removes word-final S when after the number 1.
 * Usage:
 * ```ts
 * pluralize`${applesCount} apples and ${pearsCount} pears`
 * ```
 * @param strings
 * @param expressions
 */
export function pluralize(strings: TemplateStringsArray, ...expressions: any[]) {
    let contents = "";
    const stringsArray = Array.from(strings);
    let pluralItems = false;
    for (let stringNo = 0; stringNo < stringsArray.length; stringNo += 1) {
        let chunk = stringsArray[stringNo];
        if (stringNo >= 1 && !pluralItems) {
            chunk = chunk?.replace(/(\w+)ties/i, "$1ty")?.replace(/(\w+)s\b/i, "$1");
        }
        contents += chunk;
        if (stringNo >= expressions.length) {
            break;
        }
        const nextExpression = expressions[stringNo];
        contents += String(nextExpression);
        if (typeof nextExpression === "number") {
            pluralItems = nextExpression != 1;
        } else {
            pluralItems = false;
        }
    }
    return contents;
}

/**
 * Converts a simple string transformer into a template literal tag.
 * @param mapper String transformer.
 * @returns
 */
function plainTemplateTag(mapper: (str: string) => string) {
    return function templateTag(strings: TemplateStringsArray, ...expressions: any[]) {
        let contents = "";
        const stringsArray = Array.from(strings);
        while (strings.length > 0) {
            contents += stringsArray.shift();
            if (expressions.length == 0) {
                break;
            }
            contents += String(expressions.shift());
        }
        return mapper(contents);
    };
}

/**
 * Removes line-initial and any leading or trailing whitespace from the given string.
 */
export const noindent = plainTemplateTag((str) => str.replace(/(\n+)[ \t]+/g, "$1").trim());

export function truncate(text: string, maxLength: number) {
    if (text.length > maxLength) {
        return `${text.substring(0, maxLength - 3)}...`;
    } else {
        return text;
    }
}

export function onlyif(condition: boolean) {
    return plainTemplateTag((text) => (condition ? text : ""));
}

export function escapeRegex(str: string) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

export function concatenate(...strings: string[]) {
    return strings.join(" ");
}

export function enumerate(items: any[], style?: 'comma' | 'bullets') {
    if (items.length == 0) {
        return "(none)";
    }

    switch (style) {
        case 'bullets':
            return items.map((item) => `\n• ${item}`).join("")
        default:
            return items.join(", ");
    }
}

import { truncate } from "../text.js";
import Long from "long";
import { SECOND_MS } from "../numeric.js";
import { SessionMessage } from "../../types/session-message.js";

export class MessageAgeFilter {
    private gracePeriodStart = Date.now();

    private static readonly REJECT_MESSAGES_OLDER_THAN = {
        seconds: 10,
    };

    private static readonly STARTUP_GRACE_PERIOD = {
        miliseconds: 10000,
    };

    private static parseMixedTimestampSeconds(
        timestamp: number | protobuf.Long | null | undefined
    ) {
        if (timestamp === null || timestamp === undefined) {
            return timestamp;
        }
        return typeof timestamp === "number"
            ? timestamp
            : Number(Long.fromValue(timestamp).divide(SECOND_MS).toString());
    }

    private getGracePeriodEndMS(): number {
        return this.gracePeriodStart + MessageAgeFilter.STARTUP_GRACE_PERIOD.miliseconds;
    }

    private static padMessageAge(timestamp: number | string | null) {
        return `${String(timestamp).padStart(14)}`;
    }

    public inGracePeriod() {
        return Date.now() < this.getGracePeriodEndMS();
    }

    public startGracePeriod() {
        if (this.inGracePeriod()) {
            return;
        }
        this.gracePeriodStart = Date.now();
    }

    public isMessageRecycled(message: SessionMessage): boolean {
        const receptionSeconds = new Date().getUTCSeconds().toString().padStart(2, "0");

        const messageSource = "room" in message ? "room:" : "dm  :";

        const messageBody =
            ("body" in message && JSON.stringify(truncate(message.body, 16))) || "(no body)";

        const inGracePeriod = this.inGracePeriod();

        const timestampSeconds = MessageAgeFilter.parseMixedTimestampSeconds(message.timestamp);

        if (timestampSeconds === null || timestampSeconds === undefined || timestampSeconds === 0) {
            const symbol = inGracePeriod ? "…" : "✓";
            const messageAgeString =
                message.timestamp === null
                    ? "null"
                    : message.timestamp === undefined
                        ? "undf"
                        : "zero";

            console.log(
                "[:%s] [recycle] [%s] %s message (empty) from %s %s",
                receptionSeconds,
                symbol,
                MessageAgeFilter.padMessageAge(`${messageAgeString} time`),
                messageSource,
                messageBody
            );

            return inGracePeriod;
        }

        const nowSeconds = Date.now() / SECOND_MS;

        const messageAgeSeconds = nowSeconds - timestampSeconds;

        const maxMessageAgeSeconds = MessageAgeFilter.REJECT_MESSAGES_OLDER_THAN.seconds;

        const isMessageRecycled = messageAgeSeconds > maxMessageAgeSeconds;

        console.log(
            "[:%s] [recycle] [%s] %s message %s from %s %s",
            receptionSeconds,
            inGracePeriod ? "…" : isMessageRecycled ? "x" : "✓",
            MessageAgeFilter.padMessageAge(`${Math.floor(messageAgeSeconds)}s old`),
            typeof message.timestamp === "number" ? "(int)  " : "(long) ",
            messageSource,
            messageBody
        );

        return isMessageRecycled || inGracePeriod;
    }
}

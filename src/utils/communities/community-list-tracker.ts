import fetch from "node-fetch";
import { CommunityRooms } from "./community-rooms";
import { MINUTE_MS } from "../numeric";
import { SCORoomWithServer, SCOServersJSON } from "../../types/servers-sco";

export class CommunityListTracker {
    private refreshTimerHandle: NodeJS.Timer | null = null;

    public constructor(public readonly refreshTimerDelay: number = 45 * MINUTE_MS) {}

    private static ENDPOINT = "https://sessioncommunities.online/servers.json";

    private rooms: SCORoomWithServer[] | null = null;

    public startFetchingRegularly() {
        if (this.refreshTimerHandle !== null) {
            throw new Error("Timer has been started already");
        }
        this.refreshTimerHandle = setInterval(() => this.fetchData(), this.refreshTimerDelay);
    }

    async fetchData(): Promise<boolean> {
        try {
            const response = await fetch(CommunityListTracker.ENDPOINT);
            if (!response.ok) {
                return false;
            }
            const servers: SCOServersJSON = await response.json();
            this.setFromServers(servers);
            return true;
        } catch (error) {
            console.error("Failed fetching servers", error);
            return false;
        }
    }

    private setFromServers(servers: SCOServersJSON) {
        this.rooms = [];

        for (const server of servers) {
            for (const room of server.rooms) {
                this.rooms.push({
                    ...room,
                    server,
                });
            }
        }
    }

    private checkFetched() {
        if (this.rooms === null) {
            throw new Error("Cannot poll rooms before fetching");
        }
    }

    public findCommunitiesByName(name: string): SCORoomWithServer[] {
        this.checkFetched();
        return CommunityRooms.roomsMatchingTerm(
            CommunityRooms.roomsFromBiggest(CommunityRooms.listedRoomsOnly(this.rooms!)),
            name
        );
    }
}

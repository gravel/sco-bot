import { escapeRegex, noindent, onlyif, truncate } from "../text";
import { byLargerPropFirst, bySmallerPropFirst, unique } from "../collections";
import { SOGSRoom } from "../../types/session";
import { SCORoomTag, SCORoomWithServer } from "../../types/servers-sco";
import { SessionOpenGroupV3Room } from "../../types/session-client";

export class CommunityRooms {
    public static getRoomDetailsLink(room: SCORoomWithServer): string {
        const roomIdentifier = `${room.token}+${room.server.server_id}`;
        return `https://sessioncommunities.online/#${roomIdentifier}`;
    }

    public static getRoomJoinLink(room: SCORoomWithServer): string {
        return `${room.server.base_url}/${room.token}?public_key=${room.server.pubkey}`;
    }

    public static getClientRoomId(room: SessionOpenGroupV3Room): string {
        return `${room.server.serverURL}/${room.token}`;
    }

    public static getRoomName(room: SCORoomWithServer | SOGSRoom): string {
        return room.name ?? room.token;
    }

    public static getRoomDescription(room: SCORoomWithServer | SOGSRoom): string | undefined {
        return room.description?.replace(/\s?(#[^#()@., ]+(?:,?\s*|\s+|$))+\s*.?$/, "");
    }

    public static roomIsListed(room: SCORoomWithServer): boolean {
        return !room.tags.some(
            (tag: SCORoomTag) => tag.type != "user" && ["nsfw", "test"].includes(tag.text)
        );
    }

    public static listedRoomsOnly(rooms: SCORoomWithServer[]) {
        return rooms.filter(CommunityRooms.roomIsListed);
    }

    /**
     * Filter and sort given rooms according to given search term.
     * @param rooms Community rooms in sessioncommunities.online format.
     * @param searchTerm String search term or string including flag emoji.
     * @returns Communities containing term sorted by name length and whole word match.
     * If term contains flags, communities with the given language flag are included.
     */
    public static roomsMatchingTerm(
        rooms: SCORoomWithServer[],
        searchTerm: string
    ): SCORoomWithServer[] {
        // Do not word-split searchTerm to lower the possibility of DoS attacks.

        const wholeWord = new RegExp(`\\b${escapeRegex(searchTerm.toLowerCase())}\\b`);

        const tokenMatches = rooms.filter((room) => {
            return room.token == searchTerm;
        });

        const nameMatches = rooms
            .filter((room) => {
                return CommunityRooms.getRoomName(room)
                    .toLowerCase()
                    .includes(searchTerm.toLowerCase());
            })
            .sort(bySmallerPropFirst((room) => CommunityRooms.getRoomName(room).length))
            .sort((roomA, roomB) => {
                const [nameA, nameB] = [roomA, roomB].map((room) =>
                    CommunityRooms.getRoomName(room).toLowerCase()
                );
                return (
                    Number(nameB.search(wholeWord) != -1) - Number(nameA.search(wholeWord) != -1)
                );
            });

        const languageMatches = rooms.filter((room) => {
            return room.language_flag && searchTerm.includes(room.language_flag);
        });

        const descriptionMatches = rooms.filter((room) => {
            return CommunityRooms.getRoomDescription(room)?.includes(searchTerm);
        });

        let tagMatches: SCORoomWithServer[] = [];

        if (searchTerm.startsWith("#")) {
            const searchTag = searchTerm.slice(1);
            tagMatches = rooms.filter((room) =>
                room.tags.some((roomTag) => roomTag.text == searchTag)
            );
        }

        return unique([
            ...tokenMatches,
            ...languageMatches,
            ...nameMatches,
            ...tagMatches,
            ...descriptionMatches,
        ]);
    }

    public static roomsFromBiggest(rooms: SCORoomWithServer[]) {
        return rooms.sort(byLargerPropFirst((room) => room.active_users ?? 0));
    }

    /**
     * Briefly format info about the given rooms.
     * @param rooms SOGS Rooms.
     * @return Short summary of room details for each room.
     */
    public static showRoomDetails(rooms: SCORoomWithServer[], showJoinLink: boolean): string {
        return rooms
            .map(
                (room) => noindent`
                    ${truncate(CommunityRooms.getRoomName(room), 32)} (${room.active_users} users):
                    ${CommunityRooms.getRoomDetailsLink(room)}
                    ${onlyif(showJoinLink)`join link: ${CommunityRooms.getRoomJoinLink(room)}`}
                `
            )
            .map((details) => `\n${details}\n`)
            .join("");
    }
}

export const SECOND_MS = 1e3;
export const MINUTE_MS = 60e3;
export const HOUR_MS = 3600e3;
export const DAY_MS = 24 * HOUR_MS;
export const WEEK_MS = 7 * DAY_MS;
